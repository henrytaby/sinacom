package com.senarecom.sinacom.constante;

public class Constante {


    //Estados de los formularios M-03
    public static final Long cEstadoBorrador = 1L;

    public static final Long cEstadoFinalizadoSinEnviar = 2L;

    public static final Long cEstadoEnviado = 3L;

    public static final Long cEstadoEnProcesoVerificacion = 4L;

    public static final Long cEstadoVerificado = 5L;

    public static final Long cEstadoObservado = 6L;



}
