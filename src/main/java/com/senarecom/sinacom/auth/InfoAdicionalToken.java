package com.senarecom.sinacom.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.senarecom.sinacom.domain.service.IUsuarioService;
import com.senarecom.sinacom.domain.service.UsuarioService;
import com.senarecom.sinacom.persistence.entity.Roles;
import com.senarecom.sinacom.persistence.entity.Usuario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

@Component
public class InfoAdicionalToken implements TokenEnhancer{
    private Logger logger = LoggerFactory.getLogger(UsuarioService.class);
    @Autowired
    private UsuarioService usuarioService;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        Usuario usuario = usuarioService.findByUsername(authentication.getName());
        Map<String, Object> info = new HashMap<>();
        info.put("nombres",usuario.getPersona().getNombres());
        info.put("app_paterno",usuario.getPersona().getApellidoPaterno());
        info.put("app_materno",usuario.getPersona().getApellidoMaterno());
        if((authentication.getAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals("ROLE_REPRESENTANTE_LEGAL")))||(authentication.getAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals("ROLE_OPERADOR"))))
            info.put("extras",usuario.getPersona().getRepresentanteExportador().getvOperadoresMineros());
        if((authentication.getAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals("ROLE_ENCAGADO_EXPORTACION")))||(authentication.getAuthorities().stream().anyMatch(ga -> ga.getAuthority().equals("ROLE_TECNICO_EXPORTACION"))))
            info.put("extras",usuario.getPersona().getServidorPublico().getOficinaSenarecom());


        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
        return accessToken;
    }

}
