package com.senarecom.sinacom.auth;


import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/formm03/all", "/api/formm03/page/**", "/api/uploads/img/**").permitAll()

		.antMatchers("/swagger-resources/**", "*.html", "/api/v1/swagger.json").permitAll()
		.antMatchers(HttpMethod.GET, "/api/formm03/externos").hasAnyRole("REPRESENTANTE_LEGAL", "OPERADOR")

		.antMatchers(HttpMethod.GET, "/api/formm03/encargado").hasAnyRole("ENCARGADO_EXPORTACION")
		//.antMatchers(HttpMethod.POST, "/api/formm03").hasRole("ADMIN")
		//.antMatchers("/api/formm03/**").hasRole("ENCARGADO")
		.anyRequest().authenticated()
				.and().cors().configurationSource(corsConfigurationSource());
	}


	@Bean
	public CorsConfigurationSource corsConfigurationSource(){
		CorsConfiguration config=new CorsConfiguration();
		config.setAllowedOrigins(Arrays.asList("http://localhost:4200","*"));
		config.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","OPTIONS"));
		//config.setAllowCredentials(true);
		config.setAllowedHeaders(Arrays.asList("Content-Type","Authorization"));
		UrlBasedCorsConfigurationSource source=new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**",config);
		return source;
	}
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter(){
		FilterRegistrationBean<CorsFilter> bean =new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}
}
