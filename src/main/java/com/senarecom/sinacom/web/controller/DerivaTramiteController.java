package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.FormM03;
import com.senarecom.sinacom.domain.service.DerivaTramiteService;
import com.senarecom.sinacom.domain.service.FormM03Service;
import com.senarecom.sinacom.domain.service.IM03Service;
import com.senarecom.sinacom.persistence.entity.DerivaTramite;
import com.senarecom.sinacom.persistence.entity.FormularioM03;
import com.senarecom.sinacom.pojo.IDerivacionTramiteResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class DerivaTramiteController {

    @Autowired
    private DerivaTramiteService derivaTramiteService;


    @PostMapping("/formm03/derivaTramite")
    public ResponseEntity<?> derivaTramiteTecnicoExportacion(@Valid @RequestBody DerivaTramite derivaTramite, BindingResult result, Authentication authentication) {
        Boolean derivaTramiteNew = false;
        Map<String, Object> response = new HashMap<>();
        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.BAD_REQUEST.value());
            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            derivaTramiteNew = derivaTramiteService.derivaTramiteTecnicoExportacion(derivaTramite, authentication);
            if (derivaTramiteNew == true) {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.CREATED.value());
                response.put("mensaje", "Derivación realizada con éxito!");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);

            }else {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.BAD_REQUEST.value());
                response.put("mensaje", "El id del formulario M-03 no esta activo o no esta en un estado adecuado!");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
            }

        } catch (DataAccessException e) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("errors", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @GetMapping("/derivacion/tecnicos")
    public ResponseEntity<?> derivaciónLista(Authentication authentication){
        Map<String, Object> response = new HashMap<>();
        List<IDerivacionTramiteResp> resp=derivaTramiteService.listaFuncionarios(authentication);
        if(resp==null){
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("errors", "No hay Técnicos disponibles, intente mas tarde");
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<IDerivacionTramiteResp>>(resp, HttpStatus.OK);
    }

}

