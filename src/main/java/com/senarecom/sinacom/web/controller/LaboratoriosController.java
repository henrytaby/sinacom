package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.ILaboratorioService;
import com.senarecom.sinacom.domain.service.IPaisesService;
import com.senarecom.sinacom.persistence.entity.Aduana;
import com.senarecom.sinacom.persistence.entity.Laboratorio;
import com.senarecom.sinacom.persistence.entity.Paises;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class LaboratoriosController {
    @Autowired
    private ILaboratorioService laboratorioService;

    @Autowired
    private IPaisesService paisesService;

    @GetMapping("/laboratorios")
    public List<Laboratorio> index(){
        return laboratorioService.findAll();
    }
    @GetMapping("/paises")
    public List<Paises> paises(){
        return paisesService.findAll();
    }
}
