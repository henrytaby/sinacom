package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.IEntidadAporteService;
import com.senarecom.sinacom.persistence.entity.Aduana;
import com.senarecom.sinacom.persistence.entity.EntidadAporte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class EntidadAporteController {

    @Autowired
    private IEntidadAporteService entidadAporteService;

    @GetMapping("/entidadAporte")
    public List<EntidadAporte> index(){
        return entidadAporteService.findAllActivos();
    }

}
