package com.senarecom.sinacom.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.senarecom.sinacom.domain.FormM03;
import com.senarecom.sinacom.domain.service.FormM03Service;
import com.senarecom.sinacom.domain.service.IM03Service;

import com.senarecom.sinacom.persistence.entity.FormularioM03;
import com.senarecom.sinacom.persistence.entity.FormularioM03Adjunto;
import com.senarecom.sinacom.pojo.IFormM03Adjunto;
import com.senarecom.sinacom.persistence.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.*;

@RestController
@RequestMapping("/api")
public class FormM03Controller {
    @Autowired
    private IM03Service im03Service;
    @Autowired
    private FormM03Service formM03Service;

    @GetMapping("/formm03/pagina")
    public Page<FormularioM03> getAllTutorials(
            @RequestParam(required = false) Long estado,
            @RequestParam(defaultValue = "1") int page,
          //  @RequestParam(defaultValue = "0") int pagina,
            @RequestParam(defaultValue = "10") int tam
    ) {
        page=page-1;
        Pageable paging = PageRequest.of(page, tam);
        return im03Service.findAll(paging,estado);
    }

    @GetMapping("/formm03/externos")
    public Page<FormularioM03> getTramitesPorRol(
                         @RequestParam(required = false) Long estado,
                         @RequestParam(defaultValue = "1") int page,
                         @RequestParam(defaultValue = "10") int tam,Authentication authentication){
        page=page-1;
         Pageable paginacion = PageRequest.of(page, tam);
        return  im03Service.getTramitesPorRol(authentication,paginacion,estado);
    }
    @GetMapping("/formm03/encargado")
    public Page<FormularioM03> getTramitesPorEncargado(
            @RequestParam(required = false) Long estado,
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int tam,Authentication authentication){
    page=page-1;
        Pageable paginacion = PageRequest.of(page, tam);
        return  im03Service.getTramitesPorRepresentante(authentication,paginacion,estado);
    }
    @GetMapping("/formm03/tecnico")
    public Page<FormularioM03> getTramitesPorTecnico(
            @RequestParam(required = false) Long estado,
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int tam,Authentication authentication){
        page=page-1;
        Pageable paginacion = PageRequest.of(page, tam);
        return  im03Service.getTramitesPorTecnico(authentication,paginacion,estado);
    }
    @GetMapping("/formm03/all")
    public List<FormM03> getAll(){
        return formM03Service.getAll();
    }

//    @GetMapping("/formm03/{id}")
//    public Optional<FormM03> getFormM03(@PathVariable("id")  int formM03Id){
//        return formM03Service.getFormM03(formM03Id);
//    }
//
    @GetMapping("/formm03/{id}")
    public FormularioM03 getFormM03(@PathVariable("id")  int formM03Id){
        return formM03Service.getFormM03(formM03Id);
    }

    @GetMapping("/formm03/productPresentation/{id}")
    public Optional<List<FormM03>> getByProductPresentation(@PathVariable("id") int productPresentationId){
        return formM03Service.getByProductPresentation(productPresentationId);
    }

    @PostMapping("/formm03/save")
    public FormM03 save (@RequestBody FormM03 formM03){
        return formM03Service.save(formM03);
    }



    @DeleteMapping("/formm03/delete/{id}")
    public boolean delete(@PathVariable("id") int formM03Id){
        return formM03Service.delete(formM03Id);
    }




    @PutMapping("/formm03/updateEstadoSiguiente/{id}")
    public ResponseEntity<?> updateEstadoSiguiente(@PathVariable("id") Integer id) {
        Integer res=im03Service.updateEstadoSiguiente(id);
        if(res==0){
            return new ResponseEntity<Boolean>(true, HttpStatus.OK);
        }{
            System.out.println(res);
            return new ResponseEntity<Integer>(res,HttpStatus.NOT_FOUND);
            //return false;
        }
    }
    @PutMapping("/formm03/updateEstadoAObservado/{id}")
    public boolean updateEstadoAObservado(@PathVariable("id") Integer id) {
        return im03Service.updateEstadoAObservado(id);
    }


    @GetMapping("/formm03/buscaPorId/{id}")
    public FormularioM03 buscaPorId(@PathVariable("id") Integer formM03Id){
        return (FormularioM03) im03Service.findFormularioM03ByIdFormularioM03(formM03Id);
    }

    @PostMapping("/formm03/guardaFormM03")
    public Integer saveFormM03(@RequestBody FormularioM03 formularioM03,Authentication authentication){
        return im03Service.saveFormM03(authentication,formularioM03);
    }

    @DeleteMapping("/formm03/adjunto/{id}")
    public boolean deleteAdjunto(@PathVariable("id") Long id){
        im03Service.delete(id);
        return true;
    }
    @GetMapping("/formm03/listado_requisitos/{id}")
    public List<IFormM03Adjunto> requisitos(@PathVariable("id") Long id) throws JsonProcessingException {

        return im03Service.porFormulario(id);

    }
    @PostMapping("/formm03/upload")
    public FormularioM03Adjunto upload(@RequestParam("archivo") MultipartFile archivo,@RequestParam("id_m03") Long id_m03,@RequestParam("id_documento") Long id_documento) {
        Map<String, Object> response = new HashMap<>();
        FormularioM03Adjunto formularioM03Adjunto= im03Service.findAdjunto(id_m03,id_documento);
        FormularioM03Adjunto resp = null;
        if(formularioM03Adjunto!=null){
            System.out.println("borrar");
            System.out.println(formularioM03Adjunto.getId());
            im03Service.delete(formularioM03Adjunto.getId());
        }
        System.out.println("llega por aqui");
        FormularioM03Adjunto nuevo = new FormularioM03Adjunto();
        nuevo.setIdDocumentoExportacion(id_documento);
        nuevo.setIdFormularioM03(id_m03);
        if(!archivo.isEmpty()){
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String nombreArchivo = archivo.getOriginalFilename();
            String[] parts = nombreArchivo.split("\\.");

            nombreArchivo=parts[0]+timestamp.getTime()+"."+parts[1];
            Path rutaArchivo= Paths.get("respaldos").resolve(nombreArchivo).toAbsolutePath();
            try {

                Files.copy(archivo.getInputStream(),rutaArchivo);
            } catch (IOException e) {
                System.out.println("falla");
                response.put("mensaje","Error al subir el archivo "+nombreArchivo);
 //               response.put("error",e.getMessage().concat(": ").concat(e.getCause().getMessage()));
               // return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            nuevo.setNombreArchivo(nombreArchivo);
            nuevo.setPathArchivo(rutaArchivo.toString());
             resp = im03Service.save(nuevo);
        }
        return resp;
    }

    //DistribucionRM
    @GetMapping("/formm03/distribucion_regalia/{id}")
    public List<FormularioM03DistribucionRm> ListaFormulariom03dist(@PathVariable("id") Long id){
        return im03Service.ListFormM03DistribucionRm(id);
    }
    @PostMapping("/formm03/distribucion_regalia")
    public Boolean saveFormM03DistribucionRm(@RequestBody FormularioM03DistribucionRm formularioM03DistribucionRm){
        return im03Service.saveFormM03DistribucionRm(formularioM03DistribucionRm);
    }

    @DeleteMapping("/formm03/distribucion_regalia/{id}")
    public boolean deletedistribucion (@PathVariable("id") Long id){
        return im03Service.deleteDistribucionRM(id);
    }


    //CalculoRm
    @GetMapping("/formm03/calculo_regalia/{id}")
    public List<FormularioM03CalculoRm> ListaFormulariom03calculo(@PathVariable("id") Long id){
        System.out.println(id);
        return im03Service.ListFormM03CalculoRm(id);
    }

    @PostMapping("/formm03/calculo_regalia")
    public Boolean saveFormM03CalculoRm(@RequestBody FormularioM03CalculoRm formularioM03CalculoRm){
        return im03Service.saveFormM03CalculoRm(formularioM03CalculoRm);
    }

    @DeleteMapping("/formm03/calculo_regalia/{id}")
    public boolean deleteCalculoRM (@PathVariable("id") Long id){
        return im03Service.deleteCalculoRM(id);
    }



    //calculoAporte
    @GetMapping("/formm03/calculo_aporte/{id}")
    public List<FormularioM03CalculoAporte> ListaCalculoAporteM03dist(@PathVariable("id") Long id){
        return im03Service.ListFormM03Calculo(id);
    }
    @PostMapping("/formm03/calculo_aporte")
    public Boolean saveFormM03CalculoAporte(@RequestBody FormularioM03CalculoAporte formularioM03CalculoAporte){
        return im03Service.saveFormM03CalculoAporte(formularioM03CalculoAporte);
    }
    @DeleteMapping("/formm03/calculo_aporte/{id}")
    public boolean deleteCalculoAporte (@PathVariable("id") Long id){
        return im03Service.deleteCalculoAporte(id);
    }


}
