package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.INandinaService;
import com.senarecom.sinacom.persistence.entity.Nandina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class NandinaController {

    @Autowired
    private INandinaService nandinaService;

    @GetMapping("/nandinas/{idMineral}")
    public List<Nandina> findAllByIdMineral(@PathVariable("idMineral") Long idMineral){
        return nandinaService.findAllByIdMineral(idMineral);
    }


}
