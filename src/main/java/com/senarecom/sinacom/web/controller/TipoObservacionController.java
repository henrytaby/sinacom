package com.senarecom.sinacom.web.controller;


import com.senarecom.sinacom.domain.service.ITipoObservacionService;
import com.senarecom.sinacom.persistence.entity.TipoObservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class TipoObservacionController {

    @Autowired
    private ITipoObservacionService iTipoObservacionService;

    @GetMapping("/tipo_observacion/{idseccionFormularioM03}")
    public ResponseEntity<?> ListaTipoObservacionPorSeccionDelForm(@PathVariable Long idseccionFormularioM03) {
        List<TipoObservacion> tipoObsercion = null;
        Map<String, Object> response = new HashMap<>();
        try {
            tipoObsercion = (List<TipoObservacion>) iTipoObservacionService.findByIdSeccionFormualarioM03AndActivo(idseccionFormularioM03);
        } catch (DataAccessException e) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("errors", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (tipoObsercion.size() == 0) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.NOT_FOUND.value());
            response.put("mensaje", "La sección del formulario con ID: ".concat(idseccionFormularioM03.toString().concat(" no existe en la base de datos!")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<TipoObservacion>>(tipoObsercion, HttpStatus.OK);

    }

}
