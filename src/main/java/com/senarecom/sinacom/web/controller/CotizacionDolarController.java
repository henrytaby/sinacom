package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.ICotizacionDolarService;
import com.senarecom.sinacom.persistence.entity.CotizacionDolar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CotizacionDolarController {

    @Autowired
    private ICotizacionDolarService cotizacionDolarService;

    @GetMapping("/cotizacion_dolar/{fecha}")
    public CotizacionDolar listar(@PathVariable("fecha") String fecha){
        return cotizacionDolarService.findByDate(fecha);
    }
}
