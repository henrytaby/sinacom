package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.ICotizacionMineralService;
import com.senarecom.sinacom.persistence.entity.CotizacionDolar;
import com.senarecom.sinacom.persistence.entity.CotizacionMineral;
import com.senarecom.sinacom.pojo.MineralFechaPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CotizacionMineralController {

    @Autowired
    private ICotizacionMineralService cotizacionMineralService;

    @PostMapping ("/cotizacionMineral")
    public CotizacionMineral findByIdMineralAndDate(@RequestBody MineralFechaPojo mineralFechaPojo){
        return cotizacionMineralService.findByIdMineralAndDate(mineralFechaPojo);
    }

}
