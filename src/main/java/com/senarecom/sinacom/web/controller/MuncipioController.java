package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.IMunicipioService;
import com.senarecom.sinacom.persistence.entity.Municipio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class MuncipioController {
    @Autowired
    private IMunicipioService municipioService;

    @GetMapping("/municipioDepartamento")
    public List<Municipio> index(){
        return municipioService.findAllActivos();
    }

    @GetMapping("/municipioPorDepartamento/{departamento}")
    public List<Municipio> findAllMunicipioPorDepartamento(@PathVariable("departamento") String departamento){
        return municipioService.findAllMunicipioPorDepartamento(departamento);
    }

}
