package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.FormularioM03ObservacionService;
import com.senarecom.sinacom.domain.service.IM03Service;
import com.senarecom.sinacom.persistence.entity.FormularioM03Observacion;
import com.senarecom.sinacom.persistence.entity.TipoObservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class FormularioM03ObservacionController {

    @Autowired
    private FormularioM03ObservacionService formularioM03ObservacionService;

    @GetMapping("/formm03/observacion_formulario/{id}")
    public ResponseEntity<?> ListFormularioM03Observacion(@PathVariable("id") Long id) {
        List<FormularioM03Observacion> formularioM03ObservacionNew = null;
        Map<String, Object> response = new HashMap<>();
        try {
            formularioM03ObservacionNew = formularioM03ObservacionService.ListFormularioM03Observacion(id);
        } catch (DataAccessException e) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("mensaje", "Error al realizar la consulta en la base de datos");
            response.put("errors", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (formularioM03ObservacionNew.size() == 0) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.NOT_FOUND.value());
            response.put("mensaje", "El formulario M-03 con ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<FormularioM03Observacion>>(formularioM03ObservacionNew, HttpStatus.OK);
    }

    @PostMapping("/formm03/observacion_formulario")
    public ResponseEntity<?> saveFormM03Observacion(@Valid @RequestBody FormularioM03Observacion formularioM03Observacion, BindingResult result, Authentication authentication) {
        FormularioM03Observacion formularioM03ObservacionNew = null;
        Map<String, Object> response = new HashMap<>();
        if (result.hasErrors()) {
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(err -> "El campo '" + err.getField() + "' " + err.getDefaultMessage())
                    .collect(Collectors.toList());
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.BAD_REQUEST.value());
            response.put("errors", errors);
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            formularioM03ObservacionNew = formularioM03ObservacionService.saveFormM03Observacion(formularioM03Observacion, authentication);
            if (formularioM03ObservacionNew != null) {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.CREATED.value());
                response.put("mensaje", "Observación almacenada con éxito!");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
            } else {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.BAD_REQUEST.value());
                response.put("mensaje", "Error al realizar el insert o actualización en la base de datos");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
            }

        } catch (DataAccessException e) {
            response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
            response.put("httpstatus", HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.put("mensaje", "Error al realizar el insert o actualización en la base de datos");
            response.put("errors", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/formm03/observacion_formulario/{id}")
    public ResponseEntity<?> deleteFormularioM03Observacion(@PathVariable("id") Long id, Authentication authentication) {
        Boolean resultado = false;
        Map<String, Object> response = new HashMap<>();
        try {
            resultado = formularioM03ObservacionService.deleteFormularioM03Observacion(id, authentication);
            if (resultado == true) {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.OK.value());
                response.put("mensaje", "Observación eliminada con éxito!");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
            } else {
                response.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")));
                response.put("httpstatus", HttpStatus.BAD_REQUEST.value());
                response.put("mensaje", "Error al eliminar la observación de la base de datos");
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (DataAccessException e) {
            response.put("mensaje", "Error al eliminar la observación de la base de datos");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
