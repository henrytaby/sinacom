package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.IEntidadFinancieraService;
import com.senarecom.sinacom.persistence.entity.EntidadFinanciera;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class EntidadFinancieraController {
    @Autowired
    private IEntidadFinancieraService entidadFinancieraService;

    @GetMapping("/entidadFinanciera")
    public List<EntidadFinanciera> index(){
        return entidadFinancieraService.findAll();
    }
}
