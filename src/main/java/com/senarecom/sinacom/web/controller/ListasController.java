package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.IListasService;
import com.senarecom.sinacom.domain.service.ListaServicesImp;
import com.senarecom.sinacom.persistence.entity.Minerales;
import com.senarecom.sinacom.persistence.entity.PresentacionProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ListasController {
    @Autowired
    private IListasService listasService;

    @GetMapping("/presentacion_producto")
    public List<PresentacionProducto> presentacion(){
        return listasService.findPresentacion();
    }

    @GetMapping("/minerales")
    public List<Minerales> minerales(){
        return (List<Minerales>) listasService.findMinerales();
    }
}
