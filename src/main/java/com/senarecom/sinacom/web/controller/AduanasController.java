package com.senarecom.sinacom.web.controller;

import com.senarecom.sinacom.domain.service.IAduanaService;
import com.senarecom.sinacom.persistence.entity.Aduana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class AduanasController {
    @Autowired
    private IAduanaService aduanaService;

    @GetMapping("/aduanas")
    public List<Aduana> index(){
        return aduanaService.findAll();
    }
}
