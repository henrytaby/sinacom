package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MunicipioRepository extends JpaRepository<Municipio, Long> {

    @Query("from Municipio as m where m.activo=true")
    public List<Municipio> findAllActivos();

    @Query(value ="SELECT id, codigo, municipio, provincia, departamento, activo, usuario_registro, fecha_registro, usuario_ultima_modificacion, fecha_ultima_modificacion\n" +
            "\tFROM parametrica.municipio\n" +
            "\tWHERE activo and UPPER(departamento) =UPPER(:departamento)\n" +
            "\tORDER BY municipio", nativeQuery = true)
    public List<Municipio> findAllMunicipioPorDepartamento(@Param("departamento") String departamento);

}
