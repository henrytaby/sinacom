package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03CalculoAporte;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormularioM03CalculoAporteRepository extends JpaRepository<FormularioM03CalculoAporte,Long> {
    List<FormularioM03CalculoAporte> findAllByIdFormularioM03(Long id);
}
