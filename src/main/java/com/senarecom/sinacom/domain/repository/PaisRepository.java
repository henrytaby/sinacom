package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.Paises;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaisRepository extends JpaRepository<Paises,Long> {

}
