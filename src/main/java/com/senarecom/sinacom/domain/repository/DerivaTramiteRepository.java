package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.DerivaTramite;
import com.senarecom.sinacom.pojo.IDerivacionTramiteResp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DerivaTramiteRepository extends JpaRepository<DerivaTramite,Long> {



    @Query(value = "(select \n" +
            "u.id,u.usuario,p.nombres,p.apellido_paterno,p.apellido_materno,sp.id_oficina_senarecom,os.descripcion as oficina_senarecom,ru.id_rol,\n" +
            "(SELECT count(*) FROM exportacion.deriva_tramite where id_usuario_destinatario=u.id and activo=true) as tramites\n" +
            " from seguridad.usuario u\n" +
            "LEFT JOIN seguridad.persona p on p.id=u.id_persona\n" +
            "INNER JOIN seguridad.usuario_rol ru on ru.id_usuario=u.id\n" +
            "LEFT JOIN seguridad.servidor_publico sp on sp.id_persona=p.id \n" +
            "LEFT JOIN parametrica.oficina_senarecom os on os.id=sp.id_oficina_senarecom\n" +
            "WHERE ru.id_rol=4 and id_oficina_senarecom=:origen_dep ORDER BY p.apellido_paterno)\n" +
            "UNION ALL\n" +
            "(select \n" +
            "u.id,u.usuario,p.nombres,p.apellido_paterno,p.apellido_materno,sp.id_oficina_senarecom as oficina_senarecom,os.descripcion,ru.id_rol,\n" +
            "(SELECT count(*) FROM exportacion.deriva_tramite where id_usuario_destinatario=u.id and activo=true) as tramites\n" +
            " from seguridad.usuario u\n" +
            "LEFT JOIN seguridad.persona p on p.id=u.id_persona\n" +
            "INNER JOIN seguridad.usuario_rol ru on ru.id_usuario=u.id\n" +
            "LEFT JOIN seguridad.servidor_publico sp on sp.id_persona=p.id \n" +
            "LEFT JOIN parametrica.oficina_senarecom os on os.id=sp.id_oficina_senarecom\n" +
            "WHERE ru.id_rol=4 and id_oficina_senarecom!=:origen_dep ORDER BY p.apellido_paterno)",nativeQuery = true)
    List<IDerivacionTramiteResp> listarTecnicos(Long origen_dep);
}
