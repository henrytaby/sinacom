package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03CalculoRm;
import com.senarecom.sinacom.persistence.entity.FormularioM03DistribucionRm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormularioM03DistribucionRmRepository  extends JpaRepository<FormularioM03DistribucionRm, Long> {
    List<FormularioM03DistribucionRm> findAllByIdFormularioM03(Long idFormularioM03);
}
