package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03Observacion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormularioM03ObservacionRepository extends JpaRepository<FormularioM03Observacion, Long> {
   List<FormularioM03Observacion> findAllByIdFormularioM03(Long idFormM03) ;
}
