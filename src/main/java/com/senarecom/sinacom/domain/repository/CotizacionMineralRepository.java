package com.senarecom.sinacom.domain.repository;


import com.senarecom.sinacom.persistence.entity.CotizacionMineral;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;

public interface CotizacionMineralRepository extends JpaRepository<CotizacionMineral, Long> {

    //@Query("from CotizacionMineral as cot where cot.fechaVigenciaInicial <= :fecha and cot.fechaVigenciaFinal >= :fecha and cot.mineral.id= :idMineral")
    @Query(value = "SELECT id, id_mineral, cotizacion_mineral_dolares, alicuota_externa, alicuota_interna, fecha_vigencia_inicial, fecha_vigencia_final, activo, usuario_registro, fecha_registro, usuario_ultima_modificacion, fecha_ultima_modificacion\n" +
            "\tFROM transversal.cotizacion_mineral\n" +
            "\tWHERE id_mineral= :idMineral and :fecha between fecha_vigencia_inicial and fecha_vigencia_final", nativeQuery = true)
    public CotizacionMineral findByIdMineralAndDate(@Param("idMineral") Integer idMineral, @Param("fecha") LocalDate fecha);
    //
}
