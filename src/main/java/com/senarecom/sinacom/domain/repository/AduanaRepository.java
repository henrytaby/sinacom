package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.domain.FormM03;
import com.senarecom.sinacom.persistence.entity.Aduana;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AduanaRepository  extends CrudRepository<Aduana,Long> {

    @Query("from Aduana as ad where ad.activo=true")
    public List<Aduana> findAllActivos();
}
