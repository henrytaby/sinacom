package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface M03Repository extends JpaRepository<FormularioM03,Integer> {

    Page<FormularioM03> findAllByIdEstadoFormulario(Long id,Pageable pageable);

    Page<FormularioM03> findAllByIdOperadorAndIdEstadoFormularioOrIdEstadoFormularioOrderByIdEstadoFormularioDesc(Long id,Long a,Long b,Pageable pageable);

    Page<FormularioM03> findAllByIdOficinaSenarecomAndIdEstadoFormularioOrIdEstadoFormularioOrIdEstadoFormulario(Long dep,Long a,Long b,Long c,Pageable pageable);

    Page<FormularioM03> findAllByIdOficinaSenarecomAndIdEstadoFormulario(Long dep,Long a,Pageable pageable);

    Page<FormularioM03> findAllByIdOperadorAndIdEstadoFormulario(Long id,Long a,Pageable pageable);

    Page<FormularioM03> findAllByIdEstadoFormularioOrIdEstadoFormularioOrIdEstadoFormulario(Long a,Long b,Long c,Pageable pageable);

    FormularioM03 findFormularioM03ByIdFormularioM03(Integer idFormularioM03);


}
