package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface IUsuarioRepository extends CrudRepository<Usuario,Long> {
    public Usuario findByUsuario(String username);

    @Query("select u from Usuario u where u.usuario=?1")
    public Usuario findByUsername(String username);
}
