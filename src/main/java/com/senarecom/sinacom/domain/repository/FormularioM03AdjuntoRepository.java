package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03Adjunto;
import com.senarecom.sinacom.pojo.FormularioM03AdjuntoPojo;
import com.senarecom.sinacom.pojo.IFormM03Adjunto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public interface FormularioM03AdjuntoRepository extends JpaRepository<FormularioM03Adjunto,Long> {

    Optional<FormularioM03Adjunto> findByIdFormularioM03AndIdDocumentoExportacion(Long form,Long documento);

    List<FormularioM03Adjunto> findAllByIdFormularioM03(Long id_form);

    @Query(value = "SELECT de.id ,de.descripcion, ad.id as id_formulario_m03_adjunto,ad.nombre_archivo,ad.path_archivo \n" +
            "FROM parametrica.documento_exportacion de \n" +
            "LEFT JOIN \n" +
            "(select * from public.formulario_m03_adjunto where id_formulario_m03=:idFormularioM03 and activo=true) \n" +
            "ad on ad.id_documento_exportacion=de.id\n",nativeQuery = true)
    ArrayList<IFormM03Adjunto> listarDocumentos(Long idFormularioM03);

}
