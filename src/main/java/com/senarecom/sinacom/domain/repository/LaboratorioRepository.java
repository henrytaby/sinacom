package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.Laboratorio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LaboratorioRepository extends JpaRepository<Laboratorio,Long> {

    @Query("from Laboratorio as lab where lab.activo=true")
    public List<Laboratorio> findAllActivos();
}

