package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.CotizacionDolar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;

public interface CotizacionDolarRepository extends JpaRepository<CotizacionDolar, Integer> {

    @Query("from CotizacionDolar as cot where cot.fechaVigenciaInicial <= :fecha and cot.fechaVigenciaFinal >= :fecha")
    public CotizacionDolar findByDate(@Param("fecha") LocalDate fecha);
}
