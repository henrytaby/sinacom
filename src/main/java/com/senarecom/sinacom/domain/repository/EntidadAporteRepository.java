package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.EntidadAporte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EntidadAporteRepository extends JpaRepository <EntidadAporte, Long>{

    @Query("from EntidadAporte as ea where ea.activo=true")
    public List<EntidadAporte> findAllActivos();
}
