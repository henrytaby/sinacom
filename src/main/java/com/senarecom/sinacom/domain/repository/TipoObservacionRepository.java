package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.TipoObservacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface TipoObservacionRepository extends JpaRepository <TipoObservacion, Long> {

    @Query("from TipoObservacion as tob where tob.activo = true and tob.idSeccionFormualarioM03 = :idSeccion")
    public List<TipoObservacion> findByIdSeccionFormualarioM03AndActivo(@Param("idSeccion") Long idseccionFormularioM03);
}
