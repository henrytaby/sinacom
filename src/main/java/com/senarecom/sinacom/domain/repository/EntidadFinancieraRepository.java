package com.senarecom.sinacom.domain.repository;


import com.senarecom.sinacom.persistence.entity.EntidadFinanciera;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EntidadFinancieraRepository extends CrudRepository<EntidadFinanciera, Long> {
    @Query("from EntidadFinanciera as ad where ad.activo=true")
    public List<EntidadFinanciera> findAllActivos();
}
