package com.senarecom.sinacom.domain.repository;


import com.senarecom.sinacom.persistence.entity.Minerales;
import com.senarecom.sinacom.persistence.entity.PresentacionProducto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ListasGeneralesRepository extends CrudRepository<Minerales,Long> {

    @Query("from PresentacionProducto as p where p.activo=true")
    public List<PresentacionProducto> findPresentation();

    @Query("from Minerales as m where m.activo=true order by m.descripcion")
    public List<Minerales> findMinerals();
}
