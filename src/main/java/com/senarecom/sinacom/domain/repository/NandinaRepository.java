package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.Nandina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NandinaRepository extends JpaRepository <Nandina, Long> {

     @Query(value = "SELECT id, codigo, descripcion, clasificacion_mineral, id_mineral, activo, usuario_registro, fecha_registro, usuario_ultima_modificacion, fecha_ultima_modificacion\n" +
            "\tFROM transversal.nandina\n" +
            "\twhere id_mineral =:idMineral and activo=true", nativeQuery = true)
    List <Nandina> findAllByIdMineral(@Param("idMineral") Long idMineral);
}
