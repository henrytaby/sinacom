package com.senarecom.sinacom.domain.repository;

import com.senarecom.sinacom.persistence.entity.FormularioM03CalculoRm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormularioM03CalculoRmRepository extends JpaRepository <FormularioM03CalculoRm, Long> {
    List<FormularioM03CalculoRm> findAllByIdFormularioM03(Long idFormularioM03);
}
