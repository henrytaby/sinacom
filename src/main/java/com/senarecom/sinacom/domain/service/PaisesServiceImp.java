package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.PaisRepository;
import com.senarecom.sinacom.persistence.entity.Laboratorio;
import com.senarecom.sinacom.persistence.entity.Paises;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PaisesServiceImp implements IPaisesService{
    @Autowired
    private PaisRepository paisRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Paises> findAll() {
        return (List<Paises>) paisRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Paises findById(Long id) {
        return null;
    }
}
