package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Aduana;
import com.senarecom.sinacom.persistence.entity.EntidadFinanciera;

import java.util.List;

public interface IEntidadFinancieraService {
    public List<EntidadFinanciera> findAll();
    public EntidadFinanciera findById(Long id);

}
