package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Aduana;
import com.senarecom.sinacom.persistence.entity.Paises;

import java.util.List;

public interface IPaisesService {
    public List<Paises> findAll();
    public Paises findById(Long id);
}
