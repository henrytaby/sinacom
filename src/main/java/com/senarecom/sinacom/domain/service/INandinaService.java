package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Nandina;
import com.senarecom.sinacom.persistence.entity.Paises;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface INandinaService {

    List <Nandina> findAllByIdMineral(Long idMineral);

}
