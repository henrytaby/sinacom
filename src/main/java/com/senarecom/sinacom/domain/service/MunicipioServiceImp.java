package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.MunicipioRepository;
import com.senarecom.sinacom.persistence.entity.Municipio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MunicipioServiceImp implements IMunicipioService {

    @Autowired
    private MunicipioRepository municipioRepository;

    @Override
    public List<Municipio> findAllActivos(){
        return (List<Municipio>)municipioRepository.findAllActivos();
    }

    @Override
    public List<Municipio> findAllMunicipioPorDepartamento(String departamento){
        return (List<Municipio>)municipioRepository.findAllMunicipioPorDepartamento(departamento);
    }
}
