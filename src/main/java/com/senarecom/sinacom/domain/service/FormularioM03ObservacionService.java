package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.FormularioM03DistribucionRm;
import com.senarecom.sinacom.persistence.entity.FormularioM03Observacion;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface FormularioM03ObservacionService {

    public FormularioM03Observacion saveFormM03Observacion(FormularioM03Observacion formularioM03Observacion, Authentication authentication);
    public List<FormularioM03Observacion> ListFormularioM03Observacion(Long idFormM03);
    public Boolean deleteFormularioM03Observacion(Long id, Authentication authentication);

}
