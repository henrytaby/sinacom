package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Laboratorio;

import java.util.List;

public interface ILaboratorioService {
    public List<Laboratorio> findAll();
    public Laboratorio findById(Long id);
}
