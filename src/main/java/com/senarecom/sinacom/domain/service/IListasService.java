package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Minerales;
import com.senarecom.sinacom.persistence.entity.PresentacionProducto;

import java.util.List;

public interface IListasService {
    public List<PresentacionProducto> findPresentacion();

    public List<Minerales> findMinerales();
}
