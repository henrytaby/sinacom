package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.TipoObservacion;

import java.util.List;

public interface ITipoObservacionService {
    List<TipoObservacion> findByIdSeccionFormualarioM03AndActivo(Long idseccionFormularioM03);
}
