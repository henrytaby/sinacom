package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.AduanaRepository;
import com.senarecom.sinacom.persistence.entity.Aduana;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
@Service
public class AduanaServiceImp implements IAduanaService{
    @Qualifier("aduanaRepository")
    @Autowired
    private AduanaRepository aduanaR;
    @Override
    @Transactional(readOnly = true)
    public List<Aduana> findAll() {
        return (List<Aduana>) aduanaR.findAllActivos();
    }

    @Override
    @Transactional(readOnly = true)
    public Aduana findById(Long id) {
        return aduanaR.findById(id).orElse(null);
    }
}
