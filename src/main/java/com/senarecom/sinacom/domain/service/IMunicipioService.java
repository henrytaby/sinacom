package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Municipio;

import java.util.List;

public interface IMunicipioService {
    public List<Municipio> findAllActivos();
    public List<Municipio> findAllMunicipioPorDepartamento(String departamento);
}
