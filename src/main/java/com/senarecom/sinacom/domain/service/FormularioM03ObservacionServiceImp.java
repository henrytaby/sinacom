package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.FormularioM03ObservacionRepository;
import com.senarecom.sinacom.persistence.entity.FormularioM03CalculoAporte;
import com.senarecom.sinacom.persistence.entity.FormularioM03Observacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FormularioM03ObservacionServiceImp implements FormularioM03ObservacionService {

    @Autowired
    private FormularioM03ObservacionRepository formularioM03ObservacionRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public FormularioM03Observacion saveFormM03Observacion(FormularioM03Observacion formularioM03Observacion, Authentication authentication) {
        FormularioM03Observacion formularioM03ObservacionNew = null;
        String usuarioregistro = usuarioService.findByUsername(authentication.getName()).getUsuario();
        if (formularioM03Observacion.getId() == null) {
            formularioM03Observacion.setUsuarioRegistro(usuarioregistro);
            formularioM03ObservacionNew = formularioM03ObservacionRepository.save(formularioM03Observacion);
        } else {
            FormularioM03Observacion formularioM03Observacion1 = null;
            formularioM03Observacion1 = formularioM03ObservacionRepository.findById(formularioM03Observacion.getId()).orElse(null);
            formularioM03Observacion1.setIdSeccionFormularioM03(formularioM03Observacion.getIdSeccionFormularioM03());
            formularioM03Observacion1.setIdTipoObservacion(formularioM03Observacion.getIdTipoObservacion());
            formularioM03Observacion1.setDescripcionObservacion(formularioM03Observacion.getDescripcionObservacion());
            formularioM03Observacion1.setUsuarioUltimaModificacion(usuarioregistro);
            formularioM03Observacion1.setFechaUltimaModificacion(LocalDateTime.now());
            formularioM03ObservacionNew = formularioM03ObservacionRepository.save(formularioM03Observacion1);
        }
        return formularioM03ObservacionNew;
    }


    @Override
    public List<FormularioM03Observacion> ListFormularioM03Observacion(Long idFormM03) {
        return (List<FormularioM03Observacion>) formularioM03ObservacionRepository.findAllByIdFormularioM03(idFormM03);
    }

    @Override
    public Boolean deleteFormularioM03Observacion(Long id, Authentication authentication) {
        FormularioM03Observacion formularioM03Observacion1 = null;
        String usuarioregistro = usuarioService.findByUsername(authentication.getName()).getUsuario();
        formularioM03Observacion1 = formularioM03ObservacionRepository.findById(id).orElse(null);
        if (formularioM03Observacion1 != null && formularioM03Observacion1.isActivo() == true) {
            formularioM03Observacion1.setUsuarioUltimaModificacion(usuarioregistro);
            formularioM03Observacion1.setFechaUltimaModificacion(LocalDateTime.now());
            formularioM03ObservacionRepository.save(formularioM03Observacion1);
            formularioM03ObservacionRepository.deleteById(id);
            return true;
        }
        return false;
    }
}

