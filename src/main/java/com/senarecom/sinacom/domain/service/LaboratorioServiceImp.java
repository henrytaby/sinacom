package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.LaboratorioRepository;
import com.senarecom.sinacom.persistence.entity.Laboratorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class LaboratorioServiceImp implements ILaboratorioService{

    @Autowired
    private LaboratorioRepository laboratorioR;

    @Override
    @Transactional(readOnly = true)
    public List<Laboratorio> findAll() {
        return (List<Laboratorio>) laboratorioR.findAllActivos();
    }

    @Override
    @Transactional(readOnly = true)
    public Laboratorio findById(Long id) {
        return (Laboratorio) laboratorioR.findById(id).orElse(null);
    }
}
