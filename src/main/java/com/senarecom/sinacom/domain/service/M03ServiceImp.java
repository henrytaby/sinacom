package com.senarecom.sinacom.domain.service;


import com.senarecom.sinacom.domain.repository.FormularioM03AdjuntoRepository;
import com.senarecom.sinacom.domain.repository.M03Repository;
import com.senarecom.sinacom.persistence.entity.FormularioM03;
import com.senarecom.sinacom.persistence.entity.FormularioM03Adjunto;
import com.senarecom.sinacom.pojo.IFormM03Adjunto;
import com.senarecom.sinacom.domain.repository.*;
import com.senarecom.sinacom.persistence.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.time.LocalDateTime;
import java.util.List;


@Service
public class M03ServiceImp implements IM03Service {

    @Autowired
    private M03Repository m03Repository;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private FormularioM03CalculoAporteRepository formularioM03CalculoAporteRepository;
    @Autowired
    private FormularioM03AdjuntoRepository formularioM03AdjuntoRepository;
    @Autowired
    private FormularioM03CalculoRmRepository formularioM03CalculoRmRepository;
    @Autowired
    private FormularioM03DistribucionRmRepository formularioM03DistribucionRmRepository;

    @Override
    public Page<FormularioM03> findAll(Pageable pageable, Long estado) {
        Page<FormularioM03> pageTuts;
        if (estado == null)
            pageTuts = m03Repository.findAll(pageable);
        else
            pageTuts = m03Repository.findAllByIdEstadoFormulario(estado, pageable);
        return pageTuts;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FormularioM03> getTramitesPorRol(Authentication authentication, Pageable pageable, Long estado) {
        Page<FormularioM03> forms;
        Long operador = usuarioService.findByUsername(authentication.getName()).getPersona().getRepresentanteExportador().getId_operador();
        System.out.println(operador);
        if (estado == null)
            forms = m03Repository.findAllByIdOperadorAndIdEstadoFormularioOrIdEstadoFormularioOrderByIdEstadoFormularioDesc(operador, 1L, 2L, pageable);
        else
            forms = m03Repository.findAllByIdOperadorAndIdEstadoFormulario(operador, estado, pageable);
        return forms;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FormularioM03> getTramitesPorRepresentante(Authentication authentication, Pageable pageable, Long estado) {
        Page<FormularioM03> forms;
        Long depto = usuarioService.findByUsername(authentication.getName()).getPersona().getServidorPublico().getIdOficinaSenarecom();
        if (estado == null)
            forms = m03Repository.findAllByIdOficinaSenarecomAndIdEstadoFormularioOrIdEstadoFormularioOrIdEstadoFormulario(depto, 3L, 4L, 5L, pageable);
        else
            forms = m03Repository.findAllByIdOficinaSenarecomAndIdEstadoFormulario(depto, estado, pageable);
        return forms;
    }

    @Override
    public Page<FormularioM03> getTramitesPorTecnico(Authentication authentication, Pageable pageable, Long estado) {
        Page<FormularioM03> forms;
        if (estado == null)
            forms=m03Repository.findAllByIdEstadoFormularioOrIdEstadoFormularioOrIdEstadoFormulario(4L,5L,6L,pageable);
        else
            forms=m03Repository.findAllByIdEstadoFormulario(estado,pageable);
        return forms;
    }

    public Integer updateEstadoSiguiente(Integer formM03Id) {
        FormularioM03 formularioM03 = m03Repository.findFormularioM03ByIdFormularioM03(formM03Id);
        Long vEstado = formularioM03.getIdEstadoFormulario();
        boolean vActivo = formularioM03.isActivo();
        System.out.println("a");
        if (formularioM03 == null || (vEstado !=1) || vActivo == false) {
            System.out.println("b");
            return 1;
        }

        if (vEstado == 1) {
            System.out.println("c");
            if (formularioM03.getCodigo() == null) {
                return 1;
            }
            if (formularioM03.getIdExportador() == null) {
                return 1;
            }
            if (formularioM03.getIdPais() == null) {
                return 1;
            }
            if (formularioM03.getFechaExportacion() == null) {
                return 1;
            }

            if (formularioM03.getNumeroFacturaComercial() == null) {
                return 1;
            }
            if (formularioM03.getFechaFacturaComercial() == null) {
                return 1;
            }
            if (formularioM03.getIdPresentacionProducto() == null) {
                return 2;
            }
            if (formularioM03.getIdLaboratorio() == null) {
                return 1;
            }
            if (formularioM03.getCertificadoAnalisis() == null) {
                return 1;
            }
            if (formularioM03.getIdAduana() == null) {
                return 1;
            }
            if (formularioM03.getValorFobDolares() == null) {
                return 4;
            }
            if (formularioM03.getPesoNetoSecoKilogramos() == null) {
                return 4;
            }
//            if (formularioM03.getTotalRegaliaMineraDolares() == null) {
//                return 4;
//            }
//            if (formularioM03.getTotalRegaliaMineraBolivianos() == null) {
//                return 4;
//            }
            if (formularioM03.getGastoRealizacionPorcentaje() == null) {
                return 4;
            }
            if (formularioM03.getCostoComercializacionPorcentaje() == null) {
                return 4;
            }
            if (formularioM03.getValorNetoVentaBolivianos() == null) {
                return 4;
            }if (formularioM03.getTotalDeduccionesInstitucionalesBolivianos() == null) {
                return 4;
            }if (formularioM03.getLiquidoPagableBolivianos() == null) {
                return 4;
            }
//            if (formularioM03.getTotalRegaliaMineraDeparamentalBolivianos() == null) {
//                return 4;
//            }
//            if (formularioM03.getTotalRegaliaMineraMunicipalBolivianos() == null) {
//                return 4;
//            }
//            if (formularioM03.getTotalAportesInstitucionalesBolivianos() == null) {
//                return 4;
//            }
//            if (formularioM03.getTotalPesoFinoKilogramos() == null) {
//                return 4;
//            }


            formularioM03.setIdEstadoFormulario(3L);
        }
        if (vEstado == 2) {
            formularioM03.setIdEstadoFormulario(3L);
        }
        if (vEstado == 3) {
            formularioM03.setIdEstadoFormulario(4L);
        }
        if (vEstado == 4) {
            formularioM03.setIdEstadoFormulario(5L);
        }
        m03Repository.save(formularioM03);
        return 0;
    }



    public boolean updateEstadoAObservado(Integer formM03Id) {
        FormularioM03 formularioM03 = m03Repository.findFormularioM03ByIdFormularioM03(formM03Id);
        boolean vActivo = formularioM03.isActivo();
        if (formularioM03 == null || vActivo == false) {
            return false;
        }
        Long vEstado = formularioM03.getIdEstadoFormulario();

        if (vEstado == 4) {
            formularioM03.setIdEstadoFormulario(6L);
            m03Repository.save(formularioM03);
            return true;
        }
        return false;
    }



    @Override
    @Transactional(readOnly = true)
    public FormularioM03Adjunto findAdjunto(Long idFormulario, Long idDocumentoExport) {
        return formularioM03AdjuntoRepository.findByIdFormularioM03AndIdDocumentoExportacion(idFormulario, idDocumentoExport).orElse(null);
    }

    @Override
    public FormularioM03Adjunto save(FormularioM03Adjunto formularioM03Adjunto) {
        return formularioM03AdjuntoRepository.save(formularioM03Adjunto);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        formularioM03AdjuntoRepository.deleteById(id);
    }

    @Override
    public ArrayList<IFormM03Adjunto> porFormulario(Long idFormulario) {
        return formularioM03AdjuntoRepository.listarDocumentos(idFormulario);
    }

    @Override
    @Transactional(readOnly = true)
    public FormularioM03 findFormularioM03ByIdFormularioM03(Integer idFormularioM03) {
        return (FormularioM03) m03Repository.findFormularioM03ByIdFormularioM03(idFormularioM03);
    }

    @Override
    @Transactional(readOnly = false)
    public Integer saveFormM03(Authentication authentication,FormularioM03 formularioM03) {


       // System.out.println(formularioM03.getExportador().getIdExportador());
        FormularioM03 formularioM03id = new FormularioM03();

        if (formularioM03 != null) {
            if (formularioM03.getIdFormularioM03() == null) {
                Long id_op=usuarioService.findByUsername(authentication.getName()).getPersona().getRepresentanteExportador().getId_operador();
                String rs=usuarioService.findByUsername(authentication.getName()).getPersona().getRepresentanteExportador().getvOperadoresMineros().getRazon_social();
                Long id_of=usuarioService.findByUsername(authentication.getName()).getPersona().getRepresentanteExportador().getDepartamento();
                formularioM03.setIdOficinaSenarecom(id_of);
                formularioM03.setIdOperador(id_op);
                formularioM03.setRazonSocialOperador(rs);
                formularioM03.setIdEstadoFormulario(1L);
                formularioM03.setParametricaPresentacionProducto(null);
                formularioM03.setFechaDeclaracion(LocalDateTime.now());
                System.out.println(formularioM03.getValorFobDolares());
                formularioM03id = m03Repository.save(formularioM03);
            } else {
                FormularioM03 formularioM03actualizado = m03Repository.findFormularioM03ByIdFormularioM03(formularioM03.getIdFormularioM03());
                System.out.println(formularioM03.getCodigo()+"este ");
                System.out.println();
                if(formularioM03.getCodigo()!=null) {
                    formularioM03actualizado.setCodigo(formularioM03.getCodigo());
                }
                if(formularioM03.getIdExportador()!=null)
                    formularioM03actualizado.setIdExportador(formularioM03.getIdExportador());
                if(formularioM03.getIdPais()!=null)
                    formularioM03actualizado.setIdPais(formularioM03.getIdPais());
                if(formularioM03.getRazonSocialComprador()!=null)
                    formularioM03actualizado.setRazonSocialComprador(formularioM03.getRazonSocialComprador());
                if(formularioM03.getFechaExportacion()!=null)
                    formularioM03actualizado.setFechaExportacion(formularioM03.getFechaExportacion());
                if(formularioM03.getFechaDeclaracion()!=null)
                    formularioM03actualizado.setFechaDeclaracion(formularioM03.getFechaDeclaracion());
                if(formularioM03.getFechaVerificacion()!=null)
                    formularioM03actualizado.setFechaVerificacion(formularioM03.getFechaVerificacion());
                if(formularioM03.getCotizacionDolar()!=null)
                    formularioM03actualizado.setCotizacionDolar(formularioM03.getCotizacionDolar());
                if(formularioM03.getNumeroFacturaComercial()!=null)
                    formularioM03actualizado.setNumeroFacturaComercial(formularioM03.getNumeroFacturaComercial());
                if(formularioM03.getFechaFacturaComercial()!=null)
                    formularioM03actualizado.setFechaFacturaComercial(formularioM03.getFechaFacturaComercial());
                if(formularioM03.getNumeroLote()!=null)
                    formularioM03actualizado.setNumeroLote(formularioM03.getNumeroLote());
                if(formularioM03.getIdPresentacionProducto()!=null)
                    formularioM03actualizado.setIdPresentacionProducto(formularioM03.getIdPresentacionProducto());
                if(formularioM03.getIdLaboratorio()!=null)
                    formularioM03actualizado.setIdLaboratorio(formularioM03.getIdLaboratorio());
                if(formularioM03.getCertificadoAnalisis()!=null)
                    formularioM03actualizado.setCertificadoAnalisis(formularioM03.getCertificadoAnalisis());
                if(formularioM03.getIdAduana()!=null)
                    formularioM03actualizado.setIdAduana(formularioM03.getIdAduana());
                if(formularioM03.getValorFobDolares()!=null)
                    formularioM03actualizado.setValorFobDolares(formularioM03.getValorFobDolares());
                if(formularioM03.getPesoBrutoHumedoKilogramos()!=null)
                    formularioM03actualizado.setPesoBrutoHumedoKilogramos(formularioM03.getPesoBrutoHumedoKilogramos());
                if(formularioM03.getTaraKilogramos()!=null)
                    formularioM03actualizado.setTaraKilogramos(formularioM03.getTaraKilogramos());
                if(formularioM03.getPesoNetoHumedoKilogramos()!=null)
                    formularioM03actualizado.setPesoNetoHumedoKilogramos(formularioM03.getPesoNetoHumedoKilogramos());
                if(formularioM03.getHumedadPorcentaje()!=null)
                    formularioM03actualizado.setHumedadPorcentaje(formularioM03.getHumedadPorcentaje());
                if(formularioM03.getMermaPorcentaje()!=null)
                    formularioM03actualizado.setMermaPorcentaje(formularioM03.getMermaPorcentaje());
                if(formularioM03.getMermaKilogramos()!=null)
                    formularioM03actualizado.setMermaKilogramos(formularioM03.getMermaKilogramos());
                if(formularioM03.getPesoNetoSecoKilogramos()!=null)
                    formularioM03actualizado.setPesoNetoSecoKilogramos(formularioM03.getPesoNetoSecoKilogramos());
                if(formularioM03.getTotalRegaliaMineraDolares()!=null)
                    formularioM03actualizado.setTotalRegaliaMineraDolares(formularioM03.getTotalRegaliaMineraDolares());
                if(formularioM03.getTotalRegaliaMineraBolivianos()!=null)
                    formularioM03actualizado.setTotalRegaliaMineraBolivianos(formularioM03.getTotalRegaliaMineraBolivianos());
                if(formularioM03.getGastoRealizacionPorcentaje()!=null)
                    formularioM03actualizado.setGastoRealizacionPorcentaje(formularioM03.getGastoRealizacionPorcentaje());
                if(formularioM03.getCostoComercializacionPorcentaje()!=null)
                    formularioM03actualizado.setCostoComercializacionPorcentaje(formularioM03.getCostoComercializacionPorcentaje());
                if(formularioM03.getValorNetoVentaBolivianos()!=null)
                    formularioM03actualizado.setValorNetoVentaBolivianos(formularioM03.getValorNetoVentaBolivianos());
                if(formularioM03.getTotalDeduccionesInstitucionalesBolivianos()!=null)
                    formularioM03actualizado.setTotalDeduccionesInstitucionalesBolivianos(formularioM03.getTotalDeduccionesInstitucionalesBolivianos());
                if(formularioM03.getLiquidoPagableBolivianos()!=null)
                    formularioM03actualizado.setLiquidoPagableBolivianos(formularioM03.getLiquidoPagableBolivianos());
                if(formularioM03.getTotalRegaliaMineraDeparamentalBolivianos()!=null)
                    formularioM03actualizado.setTotalRegaliaMineraDeparamentalBolivianos(formularioM03.getTotalRegaliaMineraDeparamentalBolivianos());
                if(formularioM03.getTotalRegaliaMineraMunicipalBolivianos()!=null)
                    formularioM03actualizado.setTotalRegaliaMineraMunicipalBolivianos(formularioM03.getTotalRegaliaMineraMunicipalBolivianos());
                if(formularioM03.getTotalAportesInstitucionalesBolivianos()!=null)
                    formularioM03actualizado.setTotalAportesInstitucionalesBolivianos(formularioM03.getTotalAportesInstitucionalesBolivianos());
                if(formularioM03.getIdOficinaSenarecom()!=null)
                    formularioM03actualizado.setIdOficinaSenarecom(formularioM03.getIdOficinaSenarecom());
                if(formularioM03.getIdUsuarioVerificador()!=null)
                    formularioM03actualizado.setIdUsuarioVerificador(formularioM03.getIdUsuarioVerificador());
                if(formularioM03.getIdRepresentanteLegal()!=null)
                    formularioM03actualizado.setIdRepresentanteLegal(formularioM03.getIdRepresentanteLegal());
                if(formularioM03.getIdEstadoFormulario()!=null)
                    formularioM03actualizado.setIdEstadoFormulario(formularioM03.getIdEstadoFormulario());
                if(formularioM03.getFechaRegistro()!=null)
                    formularioM03actualizado.setFechaRegistro(formularioM03.getFechaRegistro());
                if(formularioM03.getUsuarioUltimaModificacion()!=null)
                    formularioM03actualizado.setUsuarioUltimaModificacion(formularioM03.getUsuarioUltimaModificacion());
                if(formularioM03.getTotalPesoFinoKilogramos()!=null)
                    formularioM03actualizado.setTotalPesoFinoKilogramos(formularioM03.getTotalPesoFinoKilogramos());
                if(formularioM03.getRepresentanteLegal()!=null)
                    formularioM03actualizado.setRepresentanteLegal(formularioM03.getRepresentanteLegal());
                if(formularioM03.getNumeroDocumento()!=null)
                    formularioM03actualizado.setNumeroDocumento(formularioM03.getNumeroDocumento());
                if(formularioM03.getExpedicionDocumento()!=null)
                    formularioM03actualizado.setExpedicionDocumento(formularioM03.getExpedicionDocumento());
                if(formularioM03.getTotalValorBrutoVentaDolares()!=null)
                    formularioM03actualizado.setTotalValorBrutoVentaDolares(formularioM03.getTotalValorBrutoVentaDolares());
                if(formularioM03.getTotalValorBrutoVentaBolivianos()!=null)
                    formularioM03actualizado.setTotalValorBrutoVentaBolivianos(formularioM03.getTotalValorBrutoVentaBolivianos());
                formularioM03actualizado.setFechaUltimaModificacion(LocalDateTime.now());
                formularioM03id = m03Repository.save(formularioM03actualizado);
            }
            return formularioM03id.getIdFormularioM03();
        } else {
            return -1;
        }
    }


    @Override
    public Boolean saveFormM03CalculoRm(FormularioM03CalculoRm formularioM03CalculoRm) {
        if(formularioM03CalculoRm.getId()==null)
            formularioM03CalculoRmRepository.save(formularioM03CalculoRm);
        else {
            FormularioM03CalculoRm form=formularioM03CalculoRmRepository.findById(formularioM03CalculoRm.getId()).orElse(null);
            form.setIdMineral(formularioM03CalculoRm.getIdMineral());
            form.setIdNandina(formularioM03CalculoRm.getIdNandina());
            form.setLeyMineral(formularioM03CalculoRm.getLeyMineral());
            form.setLeyUnidad(formularioM03CalculoRm.getLeyUnidad());
            form.setPesoFino(formularioM03CalculoRm.getPesoFino());
            form.setPesoFinoKilogramos(formularioM03CalculoRm.getPesoFinoKilogramos());
            form.setCotizacionMineral(formularioM03CalculoRm.getCotizacionMineral());

            form.setUnidadCotizacion(formularioM03CalculoRm.getUnidadCotizacion());
            form.setValorBrutoVentaBolivianos(formularioM03CalculoRm.getValorBrutoVentaBolivianos());
            form.setValorBrutoVentaDolares(formularioM03CalculoRm.getValorBrutoVentaDolares());
            form.setAlicuotaExterna(formularioM03CalculoRm.getAlicuotaExterna());
            form.setRegaliaMineraBolivianos(formularioM03CalculoRm.getRegaliaMineraBolivianos());
            form.setRegaliaMineraDolares(formularioM03CalculoRm.getRegaliaMineraDolares());
            formularioM03CalculoRmRepository.save(form);
        }

        return true;
    }


    @Override
    public List<FormularioM03CalculoRm> ListFormM03CalculoRm(Long idM03) {
        return formularioM03CalculoRmRepository.findAllByIdFormularioM03(idM03);
    }

    @Override
    public Boolean deleteCalculoRM(Long id) {
        System.out.println("llega hasta aqui ->"+id);
        formularioM03CalculoRmRepository.deleteById(id);
        return true;
    }



    @Override
    public Boolean saveFormM03DistribucionRm(FormularioM03DistribucionRm formularioM03DistribucionRm) {
        if(formularioM03DistribucionRm.getId()==null)
            formularioM03DistribucionRmRepository.save(formularioM03DistribucionRm);
        else {
            FormularioM03DistribucionRm form=formularioM03DistribucionRmRepository.findById(formularioM03DistribucionRm.getId()).orElse(null);
            form.setIdMunicipio(formularioM03DistribucionRm.getIdMunicipio());
            form.setIdEntidadFinanciera(formularioM03DistribucionRm.getIdEntidadFinanciera());
            form.setNumeroOrden(formularioM03DistribucionRm.getNumeroOrden());
            form.setFechaDeposito(formularioM03DistribucionRm.getFechaDeposito());
            formularioM03DistribucionRmRepository.save(form);
        }
        return true;
    }

    @Override
    public List<FormularioM03DistribucionRm> ListFormM03DistribucionRm(Long idM03) {
        return formularioM03DistribucionRmRepository.findAllByIdFormularioM03(idM03);
    }

    @Override
    public Boolean deleteDistribucionRM(Long id) {
        formularioM03DistribucionRmRepository.deleteById(id);
        return true;
    }

    //CALCULO APORTE
    @Override
    public List<FormularioM03CalculoAporte> ListFormM03Calculo(Long id) {
        return formularioM03CalculoAporteRepository.findAllByIdFormularioM03(id);
    }
    @Override
    public Boolean saveFormM03CalculoAporte(FormularioM03CalculoAporte formularioM03CalculoAporte){
        if(formularioM03CalculoAporte.getId()==null)
            formularioM03CalculoAporteRepository.save(formularioM03CalculoAporte);
        else {
            FormularioM03CalculoAporte form=formularioM03CalculoAporteRepository.findById(formularioM03CalculoAporte.getId()).orElse(null);
            form.setIdentidadAporte(formularioM03CalculoAporte.getIdentidadAporte());
            form.setTipoBaseAporte(formularioM03CalculoAporte.getTipoBaseAporte());
            form.setValorBaseAporteBolivianos (formularioM03CalculoAporte.getValorBaseAporteBolivianos());

            formularioM03CalculoAporteRepository.save(form);
        }
        return true;
    }
    @Override
    public boolean deleteCalculoAporte(Long id) {
        formularioM03CalculoAporteRepository.deleteById(id);
        return true;
    }


}

