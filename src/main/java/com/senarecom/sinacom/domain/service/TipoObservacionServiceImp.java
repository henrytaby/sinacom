package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.TipoObservacionRepository;
import com.senarecom.sinacom.persistence.entity.Laboratorio;
import com.senarecom.sinacom.persistence.entity.TipoObservacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoObservacionServiceImp implements ITipoObservacionService {

    @Autowired
    private TipoObservacionRepository tipoObservacionRepository;

    @Override
    @Transactional(readOnly = true)
    public List<TipoObservacion> findByIdSeccionFormualarioM03AndActivo(Long idseccionFormularioM03){
        return (List<TipoObservacion>) tipoObservacionRepository.findByIdSeccionFormualarioM03AndActivo(idseccionFormularioM03);
    }
}
