package com.senarecom.sinacom.domain.service;


import com.senarecom.sinacom.persistence.entity.Usuario;
import org.springframework.stereotype.Service;

public interface IUsuarioService {

    public Usuario findByUsername(String username);
}
