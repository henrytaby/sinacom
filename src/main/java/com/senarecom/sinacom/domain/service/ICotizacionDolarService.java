package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.CotizacionDolar;

public interface ICotizacionDolarService {
    public CotizacionDolar findByDate(String fecha);
}
