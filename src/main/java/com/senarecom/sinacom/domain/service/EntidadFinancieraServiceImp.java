package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.EntidadFinancieraRepository;
import com.senarecom.sinacom.persistence.entity.EntidadFinanciera;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class EntidadFinancieraServiceImp implements IEntidadFinancieraService {
    @Qualifier("entidadFinancieraRepository")
    @Autowired
    private EntidadFinancieraRepository entidadFinancieraR;

    @Override
    @Transactional(readOnly = true)
    public List<EntidadFinanciera> findAll(){
        return (List<EntidadFinanciera>) entidadFinancieraR.findAllActivos();
    }

    @Override
    @Transactional(readOnly = true)
    public EntidadFinanciera findById(Long id){
        return entidadFinancieraR.findById(id).orElse(null);
    }
}
