package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.DerivaTramite;
import org.springframework.security.core.Authentication;
import com.senarecom.sinacom.pojo.IDerivacionTramiteResp;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DerivaTramiteService {

    public Boolean derivaTramiteTecnicoExportacion(DerivaTramite derivaTramite, Authentication authentication);

    @Transactional(readOnly = true)
    public List<IDerivacionTramiteResp> listaFuncionarios(Authentication authentication);
}


