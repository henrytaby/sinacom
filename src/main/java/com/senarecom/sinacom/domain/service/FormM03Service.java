package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.FormM03;
import com.senarecom.sinacom.domain.repository.FormM03Repository;
import com.senarecom.sinacom.domain.repository.M03Repository;
import com.senarecom.sinacom.persistence.entity.FormularioM03;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Table;
import java.util.List;
import java.util.Optional;

@Service
public class FormM03Service {
    @Autowired
    private FormM03Repository formM03Repository;
    @Autowired
    private M03Repository m03Repository;

    public List<FormM03> getAll(){
        return formM03Repository.getAll();
    }


    public FormularioM03 getFormM03(int formM03Id){
        return m03Repository.findById (formM03Id).orElse(null);
    }


    public Optional<List<FormM03>> getByProductPresentation(int productPresentationId){
        return formM03Repository.getByProductPresentation(productPresentationId);
    }

    public FormM03 save(FormM03 formM03){
        return formM03Repository.save(formM03);
    }

    public boolean delete(int formM03Id){
        formM03Repository.delete(formM03Id);
        return true;

    }


}
