package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.EntidadAporteRepository;
import com.senarecom.sinacom.persistence.entity.EntidadAporte;
import com.senarecom.sinacom.persistence.entity.Paises;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EntidadAporteServiceImp implements IEntidadAporteService {

    @Autowired
    private EntidadAporteRepository entidadAporteRepository;

    @Override
    @Transactional(readOnly = true)
    public List<EntidadAporte> findAllActivos(){
        return (List<EntidadAporte>) entidadAporteRepository.findAllActivos();
    }

}
