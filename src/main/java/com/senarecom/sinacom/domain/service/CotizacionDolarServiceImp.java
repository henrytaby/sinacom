package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.CotizacionDolarRepository;
import com.senarecom.sinacom.persistence.entity.CotizacionDolar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Service
public class CotizacionDolarServiceImp implements ICotizacionDolarService{
    @Autowired
    private CotizacionDolarRepository cotizacionDolarRepository;

    @Override
    public CotizacionDolar findByDate(String fecha) {
        LocalDate fechat = LocalDate.parse(fecha);

        return cotizacionDolarRepository.findByDate(fechat);
    }


}
