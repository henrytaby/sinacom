package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.Aduana;

import java.util.List;

public interface IAduanaService {
    public List<Aduana> findAll();
    public Aduana findById(Long id);

}
