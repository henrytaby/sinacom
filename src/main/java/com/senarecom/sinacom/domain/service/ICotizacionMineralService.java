package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.CotizacionMineral;
import com.senarecom.sinacom.pojo.MineralFechaPojo;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;

public interface ICotizacionMineralService {
    public CotizacionMineral findByIdMineralAndDate(MineralFechaPojo mineralFechaPojo);
}
