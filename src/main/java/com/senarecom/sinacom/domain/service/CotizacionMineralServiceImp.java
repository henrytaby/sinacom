package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.CotizacionMineralRepository;
import com.senarecom.sinacom.persistence.entity.CotizacionMineral;
import com.senarecom.sinacom.pojo.MineralFechaPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class CotizacionMineralServiceImp implements ICotizacionMineralService{

    @Autowired
    private CotizacionMineralRepository cotizacionMineralRepository;

    @Override
    public CotizacionMineral findByIdMineralAndDate(MineralFechaPojo mineralFechaPojo){
        LocalDate fechat = LocalDate.parse(mineralFechaPojo.fechaExportacion);

        return cotizacionMineralRepository.findByIdMineralAndDate(mineralFechaPojo.idMineral,fechat);
    }

}
