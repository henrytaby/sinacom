package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.NandinaRepository;
import com.senarecom.sinacom.persistence.entity.Nandina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NandinaServiceImp implements INandinaService {

    @Autowired
    private NandinaRepository nandinaRepository;


    @Override
    public List<Nandina> findAllByIdMineral(Long idMineral) {
        return nandinaRepository.findAllByIdMineral(idMineral);
    }


}
