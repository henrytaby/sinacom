package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.DerivaTramiteRepository;
import com.senarecom.sinacom.domain.repository.M03Repository;
import com.senarecom.sinacom.persistence.entity.DerivaTramite;
import com.senarecom.sinacom.persistence.entity.FormularioM03;
import com.senarecom.sinacom.pojo.IDerivacionTramiteResp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import com.senarecom.sinacom.constante.Constante;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DerivaTramiteServiceImp implements DerivaTramiteService {

    @Autowired
    private DerivaTramiteRepository derivaTramiteRepository;

    @Autowired
    private M03Repository m03Repository;

    @Autowired
    private UsuarioService usuarioService;


    @Override
    public Boolean derivaTramiteTecnicoExportacion(DerivaTramite derivaTramite, Authentication authentication) {
        FormularioM03 formularioM03 = m03Repository.findFormularioM03ByIdFormularioM03(derivaTramite.getIdFormularioM03());
        boolean vActivo = formularioM03.isActivo();
        Long vEstadoFormulario = formularioM03.getIdEstadoFormulario();
        Long idusuario = usuarioService.findByUsername(authentication.getName()).getId();
        String usuarioregistro = usuarioService.findByUsername(authentication.getName()).getUsuario();

        if (formularioM03 != null && vActivo == true && vEstadoFormulario == Constante.cEstadoEnviado) {
            formularioM03.setIdEstadoFormulario(Constante.cEstadoEnProcesoVerificacion);
            m03Repository.save(formularioM03);
            derivaTramite.setIdUsuarioRemitente(idusuario);
            derivaTramite.setUsuarioRegistro(usuarioregistro);
            derivaTramite.setIdEstadoFormulario(Constante.cEstadoEnProcesoVerificacion);
            derivaTramiteRepository.save(derivaTramite);
            return true;
        }
        return false;
    }

    @Override
    public List<IDerivacionTramiteResp> listaFuncionarios(Authentication authentication) {
        Long id_ofi=usuarioService.findByUsername(authentication.getName()).getPersona().getServidorPublico().getIdOficinaSenarecom();
        return derivaTramiteRepository.listarTecnicos(id_ofi);
    }

}

