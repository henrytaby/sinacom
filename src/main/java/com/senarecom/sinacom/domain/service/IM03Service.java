package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.FormularioM03;
import com.senarecom.sinacom.persistence.entity.FormularioM03Adjunto;
import com.senarecom.sinacom.pojo.IFormM03Adjunto;
import com.senarecom.sinacom.persistence.entity.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.util.ArrayList;
import java.util.List;


public interface IM03Service {
    public Page<FormularioM03> findAll(Pageable pageable,Long estado);
    public Page<FormularioM03> getTramitesPorRol(Authentication authentication, Pageable pageable, Long estado);
    public Page<FormularioM03> getTramitesPorRepresentante(Authentication authentication, Pageable pageable, Long estado);
    public Page<FormularioM03> getTramitesPorTecnico(Authentication authentication, Pageable pageable, Long estado);
    public Integer updateEstadoSiguiente(Integer formM03Id);
    public boolean updateEstadoAObservado(Integer formM03Id);

    public FormularioM03 findFormularioM03ByIdFormularioM03(Integer idFormularioM03);
    public Integer saveFormM03(Authentication authentication,FormularioM03 formularioM03);

    //CalculoRM
    public Boolean saveFormM03CalculoRm (FormularioM03CalculoRm formularioM03CalculoRm);
    public List<FormularioM03CalculoRm> ListFormM03CalculoRm(Long idM03);
    public Boolean deleteCalculoRM(Long id);


    //adjuntos
    public FormularioM03Adjunto findAdjunto(Long idFormulario, Long idDocumentoExport);
    public FormularioM03Adjunto save(FormularioM03Adjunto formularioM03Adjunto);
    public void delete(Long id);
    public ArrayList<IFormM03Adjunto> porFormulario(Long idFormulario);

    //distribucionRM
    public Boolean saveFormM03DistribucionRm(FormularioM03DistribucionRm formularioM03DistribucionRm);
    public List<FormularioM03DistribucionRm> ListFormM03DistribucionRm(Long idM03);
    public Boolean deleteDistribucionRM(Long id);

    //calculo_aporte
    public List<FormularioM03CalculoAporte> ListFormM03Calculo(Long id);
    public Boolean saveFormM03CalculoAporte( FormularioM03CalculoAporte formularioM03CalculoAporte);
    boolean deleteCalculoAporte(Long id);
}
