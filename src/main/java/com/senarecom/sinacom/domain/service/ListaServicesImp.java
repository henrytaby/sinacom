package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.domain.repository.ListasGeneralesRepository;
import com.senarecom.sinacom.persistence.entity.Minerales;
import com.senarecom.sinacom.persistence.entity.PresentacionProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ListaServicesImp implements IListasService{
    @Autowired
    private ListasGeneralesRepository listasGeneralesRepository;

    @Override
    public List<PresentacionProducto> findPresentacion() {
        return (List<PresentacionProducto>) listasGeneralesRepository.findPresentation();
    }

    @Override
    public List<Minerales> findMinerales() {
        return (List<Minerales>) listasGeneralesRepository.findMinerals();
    }
}
