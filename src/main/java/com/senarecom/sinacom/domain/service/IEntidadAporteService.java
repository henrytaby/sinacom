package com.senarecom.sinacom.domain.service;

import com.senarecom.sinacom.persistence.entity.EntidadAporte;

import java.util.List;

public interface IEntidadAporteService {
    public List<EntidadAporte> findAllActivos();
}
