package com.senarecom.sinacom.pojo;

public interface IFormM03Adjunto {
    Long getId();
    String getDescripcion();
    Long getId_formulario_m03_adjunto();
    String getNombre_archivo();
    String getPath_archivo();

}
