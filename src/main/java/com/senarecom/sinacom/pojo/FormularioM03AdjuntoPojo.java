package com.senarecom.sinacom.pojo;

public class FormularioM03AdjuntoPojo {

    private Long id;
    private String descripcion;
    private Long id_formulario_m03_adjunto;
    private String nombre_archivo;
    private String path_archivo;

    public  FormularioM03AdjuntoPojo(Long id, String descripcion,Long id_formulario_m03_adjunto,String nombre_archivo,String path_archivo){
        this.id=id;
        this.descripcion=descripcion;
        this.id_formulario_m03_adjunto=id_formulario_m03_adjunto;
        this.nombre_archivo=nombre_archivo;
        this.path_archivo=path_archivo;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getId_formulario_m03_adjunto() {
        return id_formulario_m03_adjunto;
    }

    public void setId_formulario_m03_adjunto(Long id_formulario_m03_adjunto) {
        this.id_formulario_m03_adjunto = id_formulario_m03_adjunto;
    }

    public String getNombre_archivo() {
        return nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }

    public String getPath_archivo() {
        return path_archivo;
    }

    public void setPath_archivo(String path_archivo) {
        this.path_archivo = path_archivo;
    }
}
