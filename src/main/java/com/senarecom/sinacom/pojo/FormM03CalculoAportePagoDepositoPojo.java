package com.senarecom.sinacom.pojo;

import javax.persistence.Column;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class FormM03CalculoAportePagoDepositoPojo {
  private Long idFormM03CalculoAporte;
  private Integer idFormularioM03;
  private Integer identidadAporte;
  private Double porcentajeAporte;
  private String tipoBaseAporte;
  private Double valorBaseAporteBolivianos;
  private Double aporteBolivianos;
  private boolean activo;
  private String usuarioRegistro;
  private LocalDateTime fechaRegistro;
  private String usuarioUltimaModificacion;
  private LocalDateTime fechaUltimaModificacion;



}
