package com.senarecom.sinacom.pojo;

public interface IDerivacionTramiteResp {
    Long getId();
    String getUsuario();
    String getNombres();
    String getApellido_paterno();
    String getApellido_materno();
    Long getId_oficina_senarecom();
    String getOficina_senarecom();
    Integer getTramites();
}
