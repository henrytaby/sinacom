package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "seguridad",name = "rol")
public class Roles implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20,name = "descripcion")
    private String nombre;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }
    private  static final long serialVersionUID = 1L;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
