package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "aduana_salida", schema = "parametrica")
public class Aduana implements Serializable {

    @Column(name = "id")
    private Long idAduana;

    @Column(nullable = false)
    private String codigo;

    private String descripcion;

    @Column(nullable = false)
    private Boolean activo;

    @Column(nullable = false)
    private String usuario_registro;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fecha_registro;

    private String usuario_ultima_modificacion;
    private Date fecha_ultima_modificacion;
    @JsonGetter("code")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @JsonGetter("description")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    @JsonGetter("active")
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @JsonGetter("userRegistry")
    public String getUsuario_registro() {
        return usuario_registro;
    }

    public void setUsuario_registro(String usuario_registro) {
        this.usuario_registro = usuario_registro;
    }
    @JsonGetter("dateRegistry")
    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
    @JsonGetter("lastModifUser")
    public String getUsuario_ultima_modificacion() {
        return usuario_ultima_modificacion;
    }

    public void setUsuario_ultima_modificacion(String usuario_ultima_modificacion) {
        this.usuario_ultima_modificacion = usuario_ultima_modificacion;
    }
    @JsonGetter("lastDateModif")
    public Date getFecha_ultima_modificacion() {
        return fecha_ultima_modificacion;
    }

    public void setFecha_ultima_modificacion(Date fecha_ultima_modificacion) {
        this.fecha_ultima_modificacion = fecha_ultima_modificacion;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public void setId(Long id) {
        this.idAduana = id;
    }

    @Id
    @JsonGetter("customsId")
    public Long getId() {
        return idAduana;
    }

    private static final long serialVersionUID = 1L;
}
