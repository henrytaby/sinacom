package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "v_representante_legal_nim_vigente",schema = "transversal")
public class VRepresentanteLegal implements Serializable {
    @Column(name = "id_representante_legal")
    private Long id;

    private Long id_operador;
    private String nombres;
    private String apellido_paterno;
    private String apellido_matero;
    private Integer numero_documento;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_operador",insertable = false,updatable = false)
    private VOperadoresMineros vOperadoresMineros;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public Long getId_operador() {
        return id_operador;
    }

    public void setId_operador(Long id_operador) {
        this.id_operador = id_operador;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_matero() {
        return apellido_matero;
    }

    public void setApellido_matero(String apellido_matero) {
        this.apellido_matero = apellido_matero;
    }

    public Integer getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(Integer numero_documento) {
        this.numero_documento = numero_documento;
    }

    public VOperadoresMineros getvOperadoresMineros() {
        return vOperadoresMineros;
    }

    public void setvOperadoresMineros(VOperadoresMineros vOperadoresMineros) {
        this.vOperadoresMineros = vOperadoresMineros;
    }
}
