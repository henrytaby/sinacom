package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
@Table(name = "entidad_aporte", schema = "parametrica")
public class EntidadAporte implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String descripcion;

    private Double porcentaje1;
    private Double porcentaje2;

    @Column(nullable = false)
    private Boolean activo;

    @Column(nullable = false)
    private String usuario_registro;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp fecha_registro;

    private String usuario_ultima_modificacion;

    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp fecha_ultima_modificacion;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPorcentaje1() {
        return porcentaje1;
    }

    public void setPorcentaje1(Double porcentaje1) {
        this.porcentaje1 = porcentaje1;
    }

    public Double getPorcentaje2() {
        return porcentaje2;
    }

    public void setPorcentaje2(Double porcentaje2) {
        this.porcentaje2 = porcentaje2;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuario_registro() {
        return usuario_registro;
    }

    public void setUsuario_registro(String usuario_registro) {
        this.usuario_registro = usuario_registro;
    }

    public Timestamp getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Timestamp fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public String getUsuario_ultima_modificacion() {
        return usuario_ultima_modificacion;
    }

    public void setUsuario_ultima_modificacion(String usuario_ultima_modificacion) {
        this.usuario_ultima_modificacion = usuario_ultima_modificacion;
    }

    public Timestamp getFecha_ultima_modificacion() {
        return fecha_ultima_modificacion;
    }

    public void setFecha_ultima_modificacion(Timestamp fecha_ultima_modificacion) {
        this.fecha_ultima_modificacion = fecha_ultima_modificacion;
    }

    private static final long serialVersionUID = 1L;

}
