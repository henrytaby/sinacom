package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "formulario_m03_calculo_aporte", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.formulario_m03_calculo_aporte SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class FormularioM03CalculoAporte implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_formulario_m03")
    private Long idFormularioM03;

    @Column(name = "id_entidad_aporte")
    private Long identidadAporte;

    @Column(name = "porcentaje_aporte")
    private Double porcentajeAporte;

    @Column(name = "tipo_base_aporte")
    private String tipoBaseAporte;

    @Column(name = "valor_base_aporte_bolivianos")
    private Double valorBaseAporteBolivianos;

    @Column(name = "aporte_bolivianos")
    private Double aporteBolivianos;

    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    public FormularioM03CalculoAporte() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }
    //RELACIONES
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    @JsonIgnore
    private FormularioM03 formularioM03;

    @ManyToOne()
    @JoinColumn(name = "id_entidad_aporte", insertable = false, updatable = false)
    private EntidadAporte entidadAporte;


    public Double getPorcentajeAporte() {
        return porcentajeAporte;
    }

    public void setPorcentajeAporte(Double porcentajeAporte) {
        this.porcentajeAporte = porcentajeAporte;
    }

    public String getTipoBaseAporte() {
        return tipoBaseAporte;
    }

    public void setTipoBaseAporte(String tipoBaseAporte) {
        this.tipoBaseAporte = tipoBaseAporte;
    }

    public Double getValorBaseAporteBolivianos() {
        return valorBaseAporteBolivianos;
    }

    public void setValorBaseAporteBolivianos(Double valorBaseAporteBolivianos) {
        this.valorBaseAporteBolivianos = valorBaseAporteBolivianos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Long idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdentidadAporte() {
        return identidadAporte;
    }

    public void setIdentidadAporte(Long identidadAporte) {
        this.identidadAporte = identidadAporte;
    }

    public Double getAporteBolivianos() {
        return aporteBolivianos;
    }

    public void setAporteBolivianos(Double aporteBolivianos) {
        this.aporteBolivianos = aporteBolivianos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }

    @JsonGetter("entidadAporte")
    public EntidadAporte getEntidadAporte() {
        return entidadAporte;
    }

    public void setEntidadAporte(EntidadAporte entidadAporte) {
        this.entidadAporte = entidadAporte;
    }

    private static final long serialVersionUID = 1L;

}
