package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "laboratorio",schema = "parametrica")
public class Laboratorio implements Serializable {

    private Long id;
    private String descripcion;
    private Boolean activo;
    private String usuario_registro;
    private Date fecha_registro;
    private String usuario_ultima_modificacion;
    private Date fecha_ultima_modificacion;
    public void setId(Long id) {
        this.id = id;
    }
    @JsonGetter("description")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    @JsonGetter("active")
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }
    @JsonGetter("userRegistry")
    public String getUsuario_registro() {
        return usuario_registro;
    }

    public void setUsuario_registro(String usuario_registro) {
        this.usuario_registro = usuario_registro;
    }
    @JsonGetter("dateRegistry")
    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }
    @JsonGetter("lastUserModify")
    public String getUsuario_ultima_modificacion() {
        return usuario_ultima_modificacion;
    }

    public void setUsuario_ultima_modificacion(String usuario_ultima_modificacion) {
        this.usuario_ultima_modificacion = usuario_ultima_modificacion;
    }

    @JsonGetter("lastDateModify")
    public Date getFecha_ultima_modificacion() {
        return fecha_ultima_modificacion;
    }

    public void setFecha_ultima_modificacion(Date fecha_ultima_modificacion) {
        this.fecha_ultima_modificacion = fecha_ultima_modificacion;
    }
    @Id
    @JsonGetter("laboratoryId")
    public Long getId() {
        return id;
    }
}
