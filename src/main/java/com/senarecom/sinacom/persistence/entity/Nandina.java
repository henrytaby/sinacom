package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


@Entity
@Table(name = "nandina", schema = "transversal")
public class Nandina implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codigo;
    private String descripcion;

    @Column(name = "clasificacion_mineral")
    private String clasificacionMineral;

    @ManyToOne
    @JoinColumn(name = "id_mineral", insertable = false, updatable = false)
    @JsonIgnore
    private Minerales mineral;

    private Boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private Timestamp fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private Timestamp fechaUltimaModificacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getClasificacionMineral() {
        return clasificacionMineral;
    }

    public void setClasificacionMineral(String clasificacionMineral) {
        this.clasificacionMineral = clasificacionMineral;
    }

    public Minerales getMineral() {
        return mineral;
    }

    public void setMineral(Minerales mineral) {
        this.mineral = mineral;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public Timestamp getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Timestamp fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    private static final long serialVersionUID = 1L;
}
