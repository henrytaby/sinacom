package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "cotizacion_mineral", schema = "parametrica")
public class CotizacionMineral implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_mineral", insertable = false, updatable = false)
    private Minerales mineral;

    @Column(name = "cotizacion_mineral_dolares")
    private Double cotizacionMineralDolares;

    @Column(name = "alicuota_externa")
    private Double alicuotaExterna;

    @Column(name = "alicuota_interna")
    private Double alicuotaInterna;

    @Column(name = "fecha_vigencia_inicial")
    private LocalDate fechaVigenciaInicial;

    @Column(name = "fecha_vigencia_final")
    private LocalDate fechaVigenciaFinal;

    private Boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Minerales getMineral() {
        return mineral;
    }

    public void setMineral(Minerales mineral) {
        this.mineral = mineral;
    }

    public Double getCotizacionMineralDolares() {
        return cotizacionMineralDolares;
    }

    public void setCotizacionMineralDolares(Double cotizacionMineralDolares) {
        this.cotizacionMineralDolares = cotizacionMineralDolares;
    }

    public Double getAlicuotaExterna() {
        return alicuotaExterna;
    }

    public void setAlicuotaExterna(Double alicuotaExterna) {
        this.alicuotaExterna = alicuotaExterna;
    }

    public Double getAlicuotaInterna() {
        return alicuotaInterna;
    }

    public void setAlicuotaInterna(Double alicuotaInterna) {
        this.alicuotaInterna = alicuotaInterna;
    }

    public LocalDate getFechaVigenciaInicial() {
        return fechaVigenciaInicial;
    }

    public void setFechaVigenciaInicial(LocalDate fechaVigenciaInicial) {
        this.fechaVigenciaInicial = fechaVigenciaInicial;
    }

    public LocalDate getFechaVigenciaFinal() {
        return fechaVigenciaFinal;
    }

    public void setFechaVigenciaFinal(LocalDate fechaVigenciaFinal) {
        this.fechaVigenciaFinal = fechaVigenciaFinal;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    private static final long serialVersionUID = 1L;


}
