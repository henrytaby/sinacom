package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "formulario_m03_calculorm", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.formulario_m03_calculorm SET activo=false WHERE id = ?")
@Where(clause = "activo=true")

public class FormularioM03CalculoRm  implements Serializable {

    public FormularioM03CalculoRm() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_formulario_m03")
    private Long idFormularioM03;

    @Column(name = "id_mineral")
    private Long idMineral;

    @Column(name = "id_nandina")
    private Long idNandina;

    @Column(name = "ley_mineral")
    private Double leyMineral;

    @Column(name = "ley_unidad")
    private String leyUnidad;

    @Column(name = "peso_fino_kilogramos")
    private Double pesoFinoKilogramos;

    @Column(name = "peso_fino")
    private Double pesoFino;

    @Column(name = "cotizacion_mineral")
    private Double cotizacionMineral;

    @Column(name = "unidad_cotizacion")
    private String unidadCotizacion;

    @Column(name = "valor_bruto_venta_dolares")
    private Double valorBrutoVentaDolares;

    @Column(name = "valor_bruto_venta_bolivianos")
    private Double valorBrutoVentaBolivianos;

    @Column(name = "alicuota_externa")
    private Double alicuotaExterna;

    @Column(name = "regalia_minera_dolares")
    private Double regaliaMineraDolares;

    @Column(name = "regalia_minera_bolivianos")
    private Double regaliaMineraBolivianos;

    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    //RELACIONES
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    private FormularioM03 formularioM03;

    @ManyToOne()
    @JoinColumn(name = "id_mineral", insertable = false, updatable = false)
    private Minerales mineral;

    @ManyToOne()
    @JoinColumn(name = "id_nandina", insertable = false, updatable = false)
    private Nandina nandina;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Long idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdMineral() {
        return idMineral;
    }

    public void setIdMineral(Long idMineral) {
        this.idMineral = idMineral;
    }

    public Long getIdNandina() {
        return idNandina;
    }

    public void setIdNandina(Long idNandina) {
        this.idNandina = idNandina;
    }

    public Double getLeyMineral() {
        return leyMineral;
    }

    public void setLeyMineral(Double leyMineral) {
        this.leyMineral = leyMineral;
    }

    public String getLeyUnidad() {
        return leyUnidad;
    }

    public void setLeyUnidad(String leyUnidad) {
        this.leyUnidad = leyUnidad;
    }

    public Double getPesoFinoKilogramos() {
        return pesoFinoKilogramos;
    }

    public void setPesoFinoKilogramos(Double pesoFinoKilogramos) {
        this.pesoFinoKilogramos = pesoFinoKilogramos;
    }

    public Double getPesoFino() {
        return pesoFino;
    }

    public void setPesoFino(Double pesoFino) {
        this.pesoFino = pesoFino;
    }

    public Double getCotizacionMineral() {
        return cotizacionMineral;
    }

    public void setCotizacionMineral(Double cotizacionMineral) {
        this.cotizacionMineral = cotizacionMineral;
    }

    public String getUnidadCotizacion() {
        return unidadCotizacion;
    }

    public void setUnidadCotizacion(String unidadCotizacion) {
        this.unidadCotizacion = unidadCotizacion;
    }

    public Double getValorBrutoVentaDolares() {
        return valorBrutoVentaDolares;
    }

    public void setValorBrutoVentaDolares(Double valorBrutoVentaDolares) {
        this.valorBrutoVentaDolares = valorBrutoVentaDolares;
    }

    public Double getValorBrutoVentaBolivianos() {
        return valorBrutoVentaBolivianos;
    }

    public void setValorBrutoVentaBolivianos(Double valorBrutoVentaBolivianos) {
        this.valorBrutoVentaBolivianos = valorBrutoVentaBolivianos;
    }

    public Double getAlicuotaExterna() {
        return alicuotaExterna;
    }

    public void setAlicuotaExterna(Double alicuotaExterna) {
        this.alicuotaExterna = alicuotaExterna;
    }

    public Double getRegaliaMineraDolares() {
        return regaliaMineraDolares;
    }

    public void setRegaliaMineraDolares(Double regaliaMineraDolares) {
        this.regaliaMineraDolares = regaliaMineraDolares;
    }

    public Double getRegaliaMineraBolivianos() {
        return regaliaMineraBolivianos;
    }

    public void setRegaliaMineraBolivianos(Double regaliaMineraBolivianos) {
        this.regaliaMineraBolivianos = regaliaMineraBolivianos;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    @JsonIgnore
    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }


    @JsonGetter("mineral")
    public Minerales getMineral() {
        return mineral;
    }

    public void setMineral(Minerales mineral) {
        this.mineral = mineral;
    }

    @JsonGetter("nandina")
    public Nandina getNandina() {
        return nandina;
    }

    public void setNandina(Nandina nandina) {
        this.nandina = nandina;
    }

    private static final long serialVersionUID = 1L;
}
