package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "formulario_m03_adjunto")
@SQLDelete(sql="UPDATE formulario_m03_adjunto SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class FormularioM03Adjunto implements Serializable {

    public FormularioM03Adjunto() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();

    }
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "id_formulario_m03",nullable = false)
    private Long idFormularioM03;
    @Column(name = "id_documento_exportacion")
    private Long idDocumentoExportacion;
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @Column(name = "path_archivo")
    private String pathArchivo;
    private Boolean activo;
    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;
    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    //RELACIONES
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    @JsonIgnore
    private FormularioM03 formularioM03;

    @OneToOne(cascade = CascadeType.DETACH,fetch = FetchType.LAZY)
    @JoinColumn(name="id_documento_exportacion",insertable=false,updatable = false)
    @JsonIgnore
    private Requisitos requisitos;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Long idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdDocumentoExportacion() {
        return idDocumentoExportacion;
    }

    public void setIdDocumentoExportacion(Long idDocumentoExportacion) {
        this.idDocumentoExportacion = idDocumentoExportacion;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getPathArchivo() {
        return pathArchivo;
    }

    public void setPathArchivo(String pathArchivo) {
        this.pathArchivo = pathArchivo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }

    public Requisitos getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(Requisitos requisitos) {
        this.requisitos = requisitos;
    }

    private static final long serialVersionUID=1L;
}
