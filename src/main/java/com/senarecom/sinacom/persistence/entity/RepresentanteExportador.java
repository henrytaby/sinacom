package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="representante_exportador",schema = "seguridad")
public class RepresentanteExportador implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    private Long id_persona;
    private Long id_operador;
    private String cargo;
    private Boolean activo;
    private Long departamento;
    @OneToOne
    @JoinColumn(name="id_persona",insertable=false,updatable = false)
    private Persona persona;

    @OneToOne
    @JoinColumn(name = "id_operador",insertable = false,updatable = false)
    private VOperadoresMineros vOperadoresMineros;

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Long getId_persona() {
        return id_persona;
    }

    public void setId_persona(Long id_persona) {
        this.id_persona = id_persona;
    }

    public Long getId_operador() {
        return id_operador;
    }

    public void setId_operador(Long id_operador) {
        this.id_operador = id_operador;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Long getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Long departamento) {
        this.departamento = departamento;
    }

    public VOperadoresMineros getvOperadoresMineros() {
        return vOperadoresMineros;
    }

    public void setvOperadoresMineros(VOperadoresMineros vOperadoresMineros) {
        this.vOperadoresMineros = vOperadoresMineros;
    }

    private static final long serialVersionUID = 1L;
}
