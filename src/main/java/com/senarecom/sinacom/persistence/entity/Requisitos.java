package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "documento_exportacion", schema = "parametrica")
public class Requisitos implements Serializable {
    private Long id;

    private String descripcion;

    @OneToOne(mappedBy = "requisitos")

    private FormularioM03Adjunto formularioM03Adjunto;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FormularioM03Adjunto getFormularioM03Adjunto() {
        return formularioM03Adjunto;
    }

    public void setFormularioM03Adjunto(FormularioM03Adjunto formularioM03Adjunto) {
        this.formularioM03Adjunto = formularioM03Adjunto;
    }
}
