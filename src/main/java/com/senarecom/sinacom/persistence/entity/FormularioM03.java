package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.*;
import java.util.List;

@Entity
@Table(name = "formulario_m03", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.formulario_m03 SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class FormularioM03 implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer idFormularioM03;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "id_exportador")
    private Integer idExportador;

    @Column(name = "numero_factura_comercial")
    private Integer numeroFacturaComercial;

    @Column(name = "certificado_analisis")
    private String certificadoAnalisis;

    @Column(name = "fecha_factura_comercial")
    private LocalDate fechaFacturaComercial;

    @Column(name = "fecha_exportacion")
    private LocalDate fechaExportacion;

    @Column(name = "razon_social_comprador")
    private String razonSocialComprador;

    @Column(name = "valor_fob_dolares")
    private Double valorFobDolares;

    @Column(name = "id_presentacion_producto")
    private Integer idPresentacionProducto;

    @Column(name = "peso_bruto_humedo_kilogramos")
    private Double pesoBrutoHumedoKilogramos;

    @Column(name = "tara_kilogramos")
    private Double taraKilogramos;

    @Column(name = "peso_neto_humedo_kilogramos")
    private Double pesoNetoHumedoKilogramos;

    @Column(name = "humedad_porcentaje")
    private Double humedadPorcentaje;

    @Column(name = "merma_porcentaje")
    private Double mermaPorcentaje;

    @Column(name = "merma_kilogramos")
    private Double mermaKilogramos;

    @Column(name = "peso_neto_seco_kilogramos")
    private Double pesoNetoSecoKilogramos;

    @Column(name = "numero_lote")
    private String numeroLote;

    @Column(name = "total_valor_bruto_venta_dolares")
    private Double totalValorBrutoVentaDolares;

    @Column(name = "total_valor_bruto_venta_bolivianos")
    private Double totalValorBrutoVentaBolivianos;

    @Column(name = "gasto_realizacion_porcentaje")
    private Double gastoRealizacionPorcentaje;

    @Column(name = "valor_neto_venta_bolivianos")
    private Double valorNetoVentaBolivianos;

    @Column(name = "costo_comercializacion_porcentaje")
    private Double costoComercializacionPorcentaje;

    @Column(name = "total_deducciones_institucionales_bolivianos")
    private Double totalDeduccionesInstitucionalesBolivianos;

    @Column(name = "liquido_pagable_bolivianos")
    private Double liquidoPagableBolivianos;

    @Column(name = "id_aduana_salida")
    private Long idAduana;

    @Column(name = "id_laboratorio")
    private  Long idLaboratorio;

    @Column(name = "id_pais_destino")
    private  Long idPais;

    @Column(name = "id_estado_formulario")
    private Long idEstadoFormulario;

    @Column(name="id_operador")
    private Long idOperador;

    @Column(name="razon_social_operador")
    private String razonSocialOperador;
    /*
        Variables que debe guardar de forma interna y que no pueden ser nulas
         */
    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "id_oficina_senarecom")
    private Long idOficinaSenarecom;

    @Column(name = "fecha_declaracion")
    private LocalDateTime fechaDeclaracion;

    @Column(name = "fecha_verificacion")
    private LocalDateTime fechaVerificacion;

    @Column(name = "cotizacion_dolar")
    private Double cotizacionDolar;

    @Column(name = "total_peso_fino_kilogramos")
    private Double totalPesoFinoKilogramos;

    @Column(name = "total_regalia_minera_dolares")
    private Double totalRegaliaMineraDolares;

    @Column(name = "total_regalia_minera_bolivianos")
    private Double totalRegaliaMineraBolivianos;

    @Column(name = "total_regalia_minera_deparamental_bolivianos")
    private Double totalRegaliaMineraDeparamentalBolivianos;

    @Column(name = "total_regalia_minera_municipal_bolivianos")
    private Double totalRegaliaMineraMunicipalBolivianos;

    @Column(name = "total_aportes_institucionales_bolivianos")
    private Double totalAportesInstitucionalesBolivianos;

    @Column(name = "id_usuario_verificador")
    private Integer idUsuarioVerificador;

    @Column(name = "id_representante_legal")
    private Integer idRepresentanteLegal;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    @Column(name = "representante_legal")
    private String representanteLegal;

    @Column(name = "numero_documento")
    private String numeroDocumento;

    @Column(name = "expedicion_documento")
    private String expedicionDocumento;

    /*
    La relacion de la tabla
     */




    @ManyToOne
    @JoinColumn(name = "id_presentacion_producto", insertable = false, updatable = false)
    private ParametricaPresentacionProducto parametricaPresentacionProducto;

    @OneToOne
    @JoinColumn(name = "id_aduana_salida", insertable = false, updatable = false)
    private Aduana aduana;

    @OneToOne
    @JoinColumn(name = "id_laboratorio", insertable = false, updatable = false)

    private Laboratorio laboratorio;

    @OneToOne
    @JoinColumn(name = "id_pais_destino", insertable = false, updatable = false)
    private Paises paises;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "formularioM03")
    private List<FormularioM03Adjunto> formularioM03Adjuntos;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "formularioM03")
    private List<FormularioM03DistribucionRm> formularioM03DistribucionRm;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "formularioM03")
    private List<FormularioM03CalculoAporte> formularioM03CalculoAportes;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "formularioM03")
    private List<FormularioM03CalculoRm>  formularioM03CalculoRms;

    @ManyToOne()
    @JoinColumn(name = "id_estado_formulario", insertable = false, updatable = false)
    private EstadoFormulario estadoFormulario;

    @OneToOne(fetch = FetchType.LAZY,mappedBy = "formularioM03")
    private DerivaTramite derivaTramite;
    public FormularioM03() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
        //this.idExportador = 4;
    }
    @JsonGetter("exporterId")
    public Integer getIdExportador() {
        return idExportador;
    }

    public void setIdExportador(Integer idExportador) {
        this.idExportador = idExportador;
    }

    @JsonGetter("active")
    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @JsonGetter("formM03Id")
    public Integer getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Integer idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }
    @JsonGetter("code")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    @JsonGetter("comercialInvoiceNumber")
    public Integer getNumeroFacturaComercial() {
        return numeroFacturaComercial;
    }

    public void setNumeroFacturaComercial(Integer numeroFacturaComercial) {
        this.numeroFacturaComercial = numeroFacturaComercial;
    }
    @JsonGetter("analysisCertificate")
    public String getCertificadoAnalisis() {
        return certificadoAnalisis;
    }

    public void setCertificadoAnalisis(String certificadoAnalisis) {
        this.certificadoAnalisis = certificadoAnalisis;
    }

    @JsonGetter("commercialInvoiceDate")
    public LocalDate getFechaFacturaComercial() {
        return fechaFacturaComercial;
    }

    public void setFechaFacturaComercial(LocalDate fechaFacturaComercial) {
        this.fechaFacturaComercial = fechaFacturaComercial;
    }
    @JsonGetter("exportDate")
    public LocalDate getFechaExportacion() {
        return fechaExportacion;
    }

    public void setFechaExportacion(LocalDate fechaExportacion) {
        this.fechaExportacion = fechaExportacion;
    }

    @JsonGetter("buyerSocialReason")
    public String getRazonSocialComprador() {
        return razonSocialComprador;
    }

    public void setRazonSocialComprador(String razonSocialComprador) {
        this.razonSocialComprador = razonSocialComprador;
    }
    @JsonGetter("valueFobDollars")
    public Double getValorFobDolares() {
        return valorFobDolares;
    }

    public void setValorFobDolares(Double valorFobDolares) {
        this.valorFobDolares = valorFobDolares;
    }

    @JsonGetter("productPresentationId")
    public Integer getIdPresentacionProducto() {
        return idPresentacionProducto;
    }

    public void setIdPresentacionProducto(Integer idPresentacionProducto) {
        this.idPresentacionProducto = idPresentacionProducto;
    }
    @JsonGetter("grossWetWeightKilograms")
    public Double getPesoBrutoHumedoKilogramos() {
        return pesoBrutoHumedoKilogramos;
    }

    public void setPesoBrutoHumedoKilogramos(Double pesoBrutoHumedoKilogramos) {
        this.pesoBrutoHumedoKilogramos = pesoBrutoHumedoKilogramos;
    }
    @JsonGetter("stateFromId")
    public Long getIdEstadoFormulario() {
        return idEstadoFormulario;
    }

    public void setIdEstadoFormulario(Long idEstadoFormulario) {
        this.idEstadoFormulario = idEstadoFormulario;
    }
    @JsonGetter("taraKilograms")
    public Double getTaraKilogramos() {
        return taraKilogramos;
    }

    public void setTaraKilogramos(Double taraKilogramos) {
        this.taraKilogramos = taraKilogramos;
    }

    @JsonGetter("wetNetWeightKilograms")
    public Double getPesoNetoHumedoKilogramos() {
        return pesoNetoHumedoKilogramos;
    }

    public void setPesoNetoHumedoKilogramos(Double pesoNetoHumedoKilogramos) {
        this.pesoNetoHumedoKilogramos = pesoNetoHumedoKilogramos;
    }
    @JsonGetter("humidityPercentage")
    public Double getHumedadPorcentaje() {
        return humedadPorcentaje;
    }

    public void setHumedadPorcentaje(Double humedadPorcentaje) {
        this.humedadPorcentaje = humedadPorcentaje;
    }

    @JsonGetter("decreasePercentage")
    public Double getMermaPorcentaje() {
        return mermaPorcentaje;
    }

    public void setMermaPorcentaje(Double mermaPorcentaje) {
        this.mermaPorcentaje = mermaPorcentaje;
    }

    @JsonGetter("decreaseKilograms")
    public Double getMermaKilogramos() {
        return mermaKilogramos;
    }

    public void setMermaKilogramos(Double mermaKilogramos) {
        this.mermaKilogramos = mermaKilogramos;
    }
    @JsonGetter("netDryWeightKilograms")
    public Double getPesoNetoSecoKilogramos() {
        return pesoNetoSecoKilogramos;
    }

    public void setPesoNetoSecoKilogramos(Double pesoNetoSecoKilogramos) {
        this.pesoNetoSecoKilogramos = pesoNetoSecoKilogramos;
    }
    @JsonGetter("lotNumber")
    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    @JsonGetter("totalGrossValueSaleDollars")
    public Double getTotalValorBrutoVentaDolares() {
        return totalValorBrutoVentaDolares;
    }

    public void setTotalValorBrutoVentaDolares(Double totalValorBrutoVentaDolares) {
        this.totalValorBrutoVentaDolares = totalValorBrutoVentaDolares;
    }
    @JsonGetter("totalGrossValueSaleBolivianos")
    public Double getTotalValorBrutoVentaBolivianos() {
        return totalValorBrutoVentaBolivianos;
    }

    public void setTotalValorBrutoVentaBolivianos(Double totalValorBrutoVentaBolivianos) {
        this.totalValorBrutoVentaBolivianos = totalValorBrutoVentaBolivianos;
    }
    @JsonGetter("expenditureRealizationPercentage")
    public Double getGastoRealizacionPorcentaje() {
        return gastoRealizacionPorcentaje;
    }

    public void setGastoRealizacionPorcentaje(Double gastoRealizacionPorcentaje) {
        this.gastoRealizacionPorcentaje = gastoRealizacionPorcentaje;
    }
    @JsonGetter("netValueSaleBolivianos")
    public Double getValorNetoVentaBolivianos() {
        return valorNetoVentaBolivianos;
    }

    public void setValorNetoVentaBolivianos(Double valorNetoVentaBolivianos) {
        this.valorNetoVentaBolivianos = valorNetoVentaBolivianos;
    }
    @JsonGetter("costMarketingPercentage")
    public Double getCostoComercializacionPorcentaje() {
        return costoComercializacionPorcentaje;
    }

    public void setCostoComercializacionPorcentaje(Double costoComercializacionPorcentaje) {
        this.costoComercializacionPorcentaje = costoComercializacionPorcentaje;
    }
    @JsonGetter("totalInstitutionalDeductionsBolivians")
    public Double getTotalDeduccionesInstitucionalesBolivianos() {
        return totalDeduccionesInstitucionalesBolivianos;
    }

    public void setTotalDeduccionesInstitucionalesBolivianos(Double totalDeduccionesInstitucionalesBolivianos) {
        this.totalDeduccionesInstitucionalesBolivianos = totalDeduccionesInstitucionalesBolivianos;
    }
    @JsonGetter("liquidPayableBolivianos")
    public Double getLiquidoPagableBolivianos() {
        return liquidoPagableBolivianos;
    }

    public void setLiquidoPagableBolivianos(Double liquidoPagableBolivianos) {
        this.liquidoPagableBolivianos = liquidoPagableBolivianos;
    }
    @JsonGetter("parametricProductPresentation")
    public ParametricaPresentacionProducto getParametricaPresentacionProducto() {
        return parametricaPresentacionProducto;
    }

    public void setParametricaPresentacionProducto(ParametricaPresentacionProducto parametricaPresentacionProducto) {
        this.parametricaPresentacionProducto = parametricaPresentacionProducto;
    }
    @JsonGetter("customsId")
    public Long getIdAduana() {
        return idAduana;
    }

    public void setIdAduana(Long idAduana) {
        this.idAduana = idAduana;
    }
    @JsonGetter("customs")
    public Aduana getAduana() {
        return aduana;
    }

    public void setAduana(Aduana aduana) {
        this.aduana = aduana;
    }
    @JsonGetter("laboratoryId")
    public Long getIdLaboratorio() {
        return idLaboratorio;
    }

    public void setIdLaboratorio(Long idLaboratorio) {
        this.idLaboratorio = idLaboratorio;
    }
    @JsonGetter("laboratory")
    public Laboratorio getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(Laboratorio laboratorio) {
        this.laboratorio = laboratorio;
    }
    @JsonGetter("countryId")
    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }
    @JsonGetter("country")
    public Paises getPaises() {
        return paises;
    }

    public Long getIdOficinaSenarecom() {
        return idOficinaSenarecom;
    }

    public void setIdOficinaSenarecom(Long idOficinaSenarecom) {
        this.idOficinaSenarecom = idOficinaSenarecom;
    }

    public List<FormularioM03Adjunto> getFormularioM03Adjuntos() {
        return formularioM03Adjuntos;
    }

    public void setFormularioM03Adjuntos(List<FormularioM03Adjunto> formularioM03Adjuntos) {
        this.formularioM03Adjuntos = formularioM03Adjuntos;
    }

    public void setPaises(Paises paises) {
        this.paises = paises;
    }

    public Long getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(Long idOperador) {
        this.idOperador = idOperador;
    }

    public String getRazonSocialOperador() {
        return razonSocialOperador;
    }

    public void setRazonSocialOperador(String razonSocialOperador) {
        this.razonSocialOperador = razonSocialOperador;
    }

    public LocalDateTime getFechaDeclaracion() {
        return fechaDeclaracion;
    }

    public void setFechaDeclaracion(LocalDateTime fechaDeclaracion) {
        this.fechaDeclaracion = fechaDeclaracion;
    }

    public LocalDateTime getFechaVerificacion() {
        return fechaVerificacion;
    }

    public void setFechaVerificacion(LocalDateTime fechaVerificacion) {
        this.fechaVerificacion = fechaVerificacion;
    }

    @JsonGetter("dollarPrice")
    public Double getCotizacionDolar() {
        return cotizacionDolar;
    }

    public void setCotizacionDolar(Double cotizacionDolar) {
        this.cotizacionDolar = cotizacionDolar;
    }

    public Double getTotalPesoFinoKilogramos() {
        return totalPesoFinoKilogramos;
    }

    public void setTotalPesoFinoKilogramos(Double totalPesoFinoKilogramos) {
        this.totalPesoFinoKilogramos = totalPesoFinoKilogramos;
    }

    public Double getTotalRegaliaMineraDolares() {
        return totalRegaliaMineraDolares;
    }

    public void setTotalRegaliaMineraDolares(Double totalRegaliaMineraDolares) {
        this.totalRegaliaMineraDolares = totalRegaliaMineraDolares;
    }

    public Double getTotalRegaliaMineraBolivianos() {
        return totalRegaliaMineraBolivianos;
    }

    public void setTotalRegaliaMineraBolivianos(Double totalRegaliaMineraBolivianos) {
        this.totalRegaliaMineraBolivianos = totalRegaliaMineraBolivianos;
    }

    public Double getTotalRegaliaMineraDeparamentalBolivianos() {
        return totalRegaliaMineraDeparamentalBolivianos;
    }

    public void setTotalRegaliaMineraDeparamentalBolivianos(Double totalRegaliaMineraDeparamentalBolivianos) {
        this.totalRegaliaMineraDeparamentalBolivianos = totalRegaliaMineraDeparamentalBolivianos;
    }

    public Double getTotalRegaliaMineraMunicipalBolivianos() {
        return totalRegaliaMineraMunicipalBolivianos;
    }

    public void setTotalRegaliaMineraMunicipalBolivianos(Double totalRegaliaMineraMunicipalBolivianos) {
        this.totalRegaliaMineraMunicipalBolivianos = totalRegaliaMineraMunicipalBolivianos;
    }

    public Double getTotalAportesInstitucionalesBolivianos() {
        return totalAportesInstitucionalesBolivianos;
    }

    public void setTotalAportesInstitucionalesBolivianos(Double totalAportesInstitucionalesBolivianos) {
        this.totalAportesInstitucionalesBolivianos = totalAportesInstitucionalesBolivianos;
    }

    public Integer getIdUsuarioVerificador() {
        return idUsuarioVerificador;
    }

    public void setIdUsuarioVerificador(Integer idUsuarioVerificador) {
        this.idUsuarioVerificador = idUsuarioVerificador;
    }

    public Integer getIdRepresentanteLegal() {
        return idRepresentanteLegal;
    }

    public void setIdRepresentanteLegal(Integer idRepresentanteLegal) {
        this.idRepresentanteLegal = idRepresentanteLegal;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public String getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(String representanteLegal) {
        this.representanteLegal = representanteLegal;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getExpedicionDocumento() {
        return expedicionDocumento;
    }

    public List<FormularioM03DistribucionRm> getFormularioM03DistribucionRm() {
        return formularioM03DistribucionRm;
    }

    public List<FormularioM03CalculoAporte> getFormularioM03CalculoAportes() {
        return formularioM03CalculoAportes;
    }

    public List<FormularioM03CalculoRm> getFormularioM03CalculoRms() {
        return formularioM03CalculoRms;
    }

    public void setFormularioM03CalculoRms(List<FormularioM03CalculoRm> formularioM03CalculoRms) {
        this.formularioM03CalculoRms = formularioM03CalculoRms;
    }

    public void setFormularioM03CalculoAportes(List<FormularioM03CalculoAporte> formularioM03CalculoAportes) {
        this.formularioM03CalculoAportes = formularioM03CalculoAportes;
    }

    public void setFormularioM03DistribucionRm(List<FormularioM03DistribucionRm> formularioM03DistribucionRm) {
        this.formularioM03DistribucionRm = formularioM03DistribucionRm;
    }
    @JsonGetter()
    public EstadoFormulario getEstadoFormulario() {
        return estadoFormulario;
    }

    public void setEstadoFormulario(EstadoFormulario estadoFormulario) {
        this.estadoFormulario = estadoFormulario;
    }

    public void setExpedicionDocumento(String expedicionDocumento) {
        this.expedicionDocumento = expedicionDocumento;
    }

    public DerivaTramite getDerivaTramite() {
        return derivaTramite;
    }

    public void setDerivaTramite(DerivaTramite derivaTramite) {
        this.derivaTramite = derivaTramite;
    }
}
