package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "formulario_m03_distribucionrm", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.formulario_m03_distribucionrm SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class FormularioM03DistribucionRm implements Serializable {


    public FormularioM03DistribucionRm() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_formulario_m03")
    private Long idFormularioM03;

    @Column(name = "id_municipio")
    private Long idMunicipio;

    @Column(name = "importe_rm_bolivianos")
    private Double importeRmBolivianos;

    @Column(name = "aporte_municipal_bolivianos")
    private Double aporteMunicipalBolivianos;

    @Column(name = "aporte_departamental_bolivianos")
    private Double aporteDepartamentalBolivianos;

    @Column(name = "id_entidad_financiera")
    private Long idEntidadFinanciera;

    @Column(name = "numero_orden")
    private String numeroOrden;

    @Column(name = "fecha_deposito")
    private LocalDate fechaDeposito;

    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;


//    //RELACIONES
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    @JsonIgnore
    private FormularioM03 formularioM03;


    @ManyToOne()
    @JoinColumn(name = "id_municipio", insertable=false, updatable = false)
    private Municipio municipio;

    @ManyToOne()
    @JoinColumn(name = "id_entidad_financiera", insertable=false, updatable = false)
    private EntidadFinanciera entidadFinanciera;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getImporteRmBolivianos() {
        return importeRmBolivianos;
    }

    public void setImporteRmBolivianos(Double importeRmBolivianos) {
        this.importeRmBolivianos = importeRmBolivianos;
    }

    public Double getAporteMunicipalBolivianos() {
        return aporteMunicipalBolivianos;
    }

    public void setAporteMunicipalBolivianos(Double aporteMunicipalBolivianos) {
        this.aporteMunicipalBolivianos = aporteMunicipalBolivianos;
    }

    public Double getAporteDepartamentalBolivianos() {
        return aporteDepartamentalBolivianos;
    }

    public void setAporteDepartamentalBolivianos(Double aporteDepartamentalBolivianos) {
        this.aporteDepartamentalBolivianos = aporteDepartamentalBolivianos;
    }

    public String getNumeroOrden() {
        return numeroOrden;
    }

    public void setNumeroOrden(String numeroOrden) {
        this.numeroOrden = numeroOrden;
    }

    public LocalDate getFechaDeposito() {
        return fechaDeposito;
    }

    public void setFechaDeposito(LocalDate fechaDeposito) {
        this.fechaDeposito = fechaDeposito;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Long getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Long idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdMunicipio() {
        return idMunicipio;
    }

    public void setIdMunicipio(Long idMunicipio) {
        this.idMunicipio = idMunicipio;
    }

    public Long getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Long idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }
    @JsonIgnore
    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }

    @JsonGetter("municipio")
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }


    @JsonGetter("entidadFinanciera")
    public EntidadFinanciera getEntidadFinanciera() {
        return entidadFinanciera;
    }

    public void setEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        this.entidadFinanciera = entidadFinanciera;
    }

    private static final long serialVersionUID = 1L;
}
