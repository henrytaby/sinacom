package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "deriva_tramite", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.deriva_tramite SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class DerivaTramite implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Range(min = 1)
    @Column(name = "id_formulario_m03")
    private Integer idFormularioM03;


    @Range(min = 1)
    @Column(name = "id_usuario_remitente")
    private Long idUsuarioRemitente;

    @NotNull
    @Range(min = 1)
    @Column(name = "id_usuario_destinatario")
    private Long idUsuarioDestinatario;

    @NotNull
    @Column(name = "fecha_envio")
    private LocalDateTime fechaEnvio;

    @Column(name = "fecha_recepcion")
    private LocalDateTime fechaRecepcion;

    @Column(name = "fecha_conclusion")
    private LocalDateTime fechaConclusion;


    @Range(min = 1)
    @Column(name = "id_estado_formulario")
    private Long idEstadoFormulario;

    private boolean activo;

    @NotNull
    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @NotNull
    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    public DerivaTramite() {
        this.fechaEnvio = LocalDateTime.now();
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }
    //RELACIONES
    @OneToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    @JsonIgnore
    private FormularioM03 formularioM03;

    @ManyToOne()
    @JoinColumn(name = "id_estado_formulario", insertable = false, updatable = false)
    private EstadoFormulario estadoFormulario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Integer idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdUsuarioRemitente() {
        return idUsuarioRemitente;
    }

    public void setIdUsuarioRemitente(Long idUsuarioRemitente) {
        this.idUsuarioRemitente = idUsuarioRemitente;
    }

    public Long getIdUsuarioDestinatario() {
        return idUsuarioDestinatario;
    }

    public void setIdUsuarioDestinatario(Long idUsuarioDestinatario) {
        this.idUsuarioDestinatario = idUsuarioDestinatario;
    }

    public LocalDateTime getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(LocalDateTime fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public LocalDateTime getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(LocalDateTime fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public LocalDateTime getFechaConclusion() {
        return fechaConclusion;
    }

    public void setFechaConclusion(LocalDateTime fechaConclusion) {
        this.fechaConclusion = fechaConclusion;
    }

    public Long getIdEstadoFormulario() {
        return idEstadoFormulario;
    }

    public void setIdEstadoFormulario(Long idEstadoFormulario) {
        this.idEstadoFormulario = idEstadoFormulario;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }

    public EstadoFormulario getEstadoFormulario() {
        return estadoFormulario;
    }

    public void setEstadoFormulario(EstadoFormulario estadoFormulario) {
        this.estadoFormulario = estadoFormulario;
    }

    private static final long serialVersionUID = 1L;

}
