package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "pago_aporte_deposito", schema = "exportacion")
public class PagoAporteDeposito implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long idPagoAporteDeposito;

    @Column(name = "id_formulario_m03_calculo_aporte")
    private Integer idFormM03CalculoAporte;

    @Column(name = "importe_bolivianos")
    private Double importeBolivianos;

    @Column(name = "id_entidad_financiera")
    private Integer idEntidadFinanciera;

    @Column(name = "numero_cuenta")
    private String numeroCuenta;

    @Column(name = "numero_deposito")
    private String numeroDeposito;

    @Column(name = "fecha_deposito")
    private LocalDate fechaDeposito;

    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;

    public PagoAporteDeposito() {
        this.activo = true;
        this.usuarioRegistro = "SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }

    public Long getIdPagoAporteDeposito() {
        return idPagoAporteDeposito;
    }

    public void setIdPagoAporteDeposito(Long idPagoAporteDeposito) {
        this.idPagoAporteDeposito = idPagoAporteDeposito;
    }

    public Integer getIdFormM03CalculoAporte() {
        return idFormM03CalculoAporte;
    }

    public void setIdFormM03CalculoAporte(Integer idFormM03CalculoAporte) {
        this.idFormM03CalculoAporte = idFormM03CalculoAporte;
    }

    public Double getImporteBolivianos() {
        return importeBolivianos;
    }

    public void setImporteBolivianos(Double importeBolivianos) {
        this.importeBolivianos = importeBolivianos;
    }

    public Integer getIdEntidadFinanciera() {
        return idEntidadFinanciera;
    }

    public void setIdEntidadFinanciera(Integer idEntidadFinanciera) {
        this.idEntidadFinanciera = idEntidadFinanciera;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroDeposito() {
        return numeroDeposito;
    }

    public void setNumeroDeposito(String numeroDeposito) {
        this.numeroDeposito = numeroDeposito;
    }

    public LocalDate getFechaDeposito() {
        return fechaDeposito;
    }

    public void setFechaDeposito(LocalDate fechaDeposito) {
        this.fechaDeposito = fechaDeposito;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    private static final long serialVersionUID = 1L;
}
