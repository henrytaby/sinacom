package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "mineral",schema = "transversal")
public class Minerales implements Serializable{

    private Long id;
    private String descripcion;
    private String simbolo;

    @Column(name = "unidad_cotizacion")
    private String unidadCotizacion;

    private Boolean activo;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    public String getUnidadCotizacion() {
        return unidadCotizacion;
    }

    public void setUnidadCotizacion(String unidadCotizacion) {
        this.unidadCotizacion = unidadCotizacion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    private static final long serialVersionUID = 1L;
}
