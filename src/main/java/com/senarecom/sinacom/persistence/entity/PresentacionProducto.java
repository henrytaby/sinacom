package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "presentacion_producto",schema = "parametrica")
public class PresentacionProducto implements Serializable {

    private Long id;
    private String descripcion;
    private Boolean activo;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    private static final long serialVersionUID = 1L;
}
