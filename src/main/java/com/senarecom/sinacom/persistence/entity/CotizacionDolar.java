package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "cotizacion_dolar",schema = "transversal")
public class CotizacionDolar implements Serializable {

    private Long id;
    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    @Column(name = "valor_bolivianos")
    private Double valorBolivianos;

    @Column(name = "fecha_vigencia_inicial")
    private LocalDate fechaVigenciaInicial;

    @Column(name = "fecha_vigencia_final")
    private LocalDate fechaVigenciaFinal;

    public Double getValorBolivianos() {
        return valorBolivianos;
    }

    public void setValorBolivianos(Double valorBolivianos) {
        this.valorBolivianos = valorBolivianos;
    }

    public  LocalDate getFechaVigenciaInicial() {
        return fechaVigenciaInicial;
    }

    public void setFechaVigenciaInicial(LocalDate fechaVigenciaInicial) {
        this.fechaVigenciaInicial = fechaVigenciaInicial;
    }

    public LocalDate getFechaVigenciaFinal() {
        return fechaVigenciaFinal;
    }

    public void setFechaVigenciaFinal(LocalDate fechaVigenciaFinal) {
        this.fechaVigenciaFinal = fechaVigenciaFinal;
    }

    private static final long serialVersionUID = 1L;
}
