package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "formulario_m03_observacion", schema = "exportacion")
@SQLDelete(sql="UPDATE exportacion.formulario_m03_observacion SET activo=false WHERE id = ?")
@Where(clause = "activo=true")
public class FormularioM03Observacion implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Range(min = 1)
    @Column(name = "id_formulario_m03")
    private Long idFormularioM03;

    @NotNull
    @Range(min = 1)
    @Column(name = "id_seccion_formulario_m03")
    private Long idSeccionFormularioM03;

    @NotNull
    @Range(min = 1)
    @Column(name = "id_tipo_observacion")
    private Long idTipoObservacion;

    @NotNull
    @Column(name = "descripcion_observacion")
    private String descripcionObservacion;

    private boolean activo;

    @Column(name = "usuario_registro")
    private String usuarioRegistro;

    @Column(name = "fecha_registro")
    private LocalDateTime fechaRegistro;

    @Column(name = "usuario_ultima_modificacion")
    private String usuarioUltimaModificacion;

    @Column(name = "fecha_ultima_modificacion")
    private LocalDateTime fechaUltimaModificacion;


//    //RELACIONES

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_formulario_m03",insertable=false,updatable = false)
    @JsonIgnore
    private FormularioM03 formularioM03;

    @ManyToOne()
    @JoinColumn(name = "id_seccion_formulario_m03", insertable=false, updatable = false)
    private SeccionFormularioM03 seccionFormularioM03;

    @ManyToOne()
    @JoinColumn(name = "id_tipo_observacion", insertable=false, updatable = false)
    private TipoObservacion tipoObservacion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdFormularioM03() {
        return idFormularioM03;
    }

    public void setIdFormularioM03(Long idFormularioM03) {
        this.idFormularioM03 = idFormularioM03;
    }

    public Long getIdSeccionFormularioM03() {
        return idSeccionFormularioM03;
    }

    public void setIdSeccionFormularioM03(Long idSeccionFormularioM03) {
        this.idSeccionFormularioM03 = idSeccionFormularioM03;
    }

    public Long getIdTipoObservacion() {
        return idTipoObservacion;
    }

    public void setIdTipoObservacion(Long idTipoObservacion) {
        this.idTipoObservacion = idTipoObservacion;
    }

    public String getDescripcionObservacion() {
        return descripcionObservacion;
    }

    public void setDescripcionObservacion(String descripcionObservacion) {
        this.descripcionObservacion = descripcionObservacion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public LocalDateTime getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDateTime fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getUsuarioUltimaModificacion() {
        return usuarioUltimaModificacion;
    }

    public void setUsuarioUltimaModificacion(String usuarioUltimaModificacion) {
        this.usuarioUltimaModificacion = usuarioUltimaModificacion;
    }

    public LocalDateTime getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(LocalDateTime fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public FormularioM03 getFormularioM03() {
        return formularioM03;
    }

    public void setFormularioM03(FormularioM03 formularioM03) {
        this.formularioM03 = formularioM03;
    }

    public SeccionFormularioM03 getSeccionFormularioM03() {
        return seccionFormularioM03;
    }

    public void setSeccionFormularioM03(SeccionFormularioM03 seccionFormularioM03) {
        this.seccionFormularioM03 = seccionFormularioM03;
    }

    public TipoObservacion getTipoObservacion() {
        return tipoObservacion;
    }

    public void setTipoObservacion(TipoObservacion tipoObservacion) {
        this.tipoObservacion = tipoObservacion;
    }

    public FormularioM03Observacion() {
        this.activo = true;
        this.usuarioRegistro="SENARECOM";
        this.fechaRegistro = LocalDateTime.now();
    }

    private static final long serialVersionUID = 1L;
}
