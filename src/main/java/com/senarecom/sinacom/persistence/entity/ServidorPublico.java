package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name="servidor_publico",schema = "seguridad")
public class ServidorPublico implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_persona")
    private Long idPersona;

    @Column(name="id_oficina_senarecom")
    private Long idOficinaSenarecom;

    @Column(name="id_cargo")
    private Long idCargo;
    private Boolean activo;

    @OneToOne
    @JoinColumn(name = "id_persona",insertable=false,updatable = false)
    private Persona persona;

    @OneToOne
    @JoinColumn(name = "id_oficina_senarecom",insertable=false,updatable = false)
    private OficinaSenarecom oficinaSenarecom;

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }



    public Long getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Long idPersona) {
        this.idPersona = idPersona;
    }

    public Long getIdOficinaSenarecom() {
        return idOficinaSenarecom;
    }

    public void setIdOficinaSenarecom(Long idOficinaSenarecom) {
        this.idOficinaSenarecom = idOficinaSenarecom;
    }

    public Long getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Long idCargo) {
        this.idCargo = idCargo;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public OficinaSenarecom getOficinaSenarecom() {
        return oficinaSenarecom;
    }

    public void setOficinaSenarecom(OficinaSenarecom oficinaSenarecom) {
        this.oficinaSenarecom = oficinaSenarecom;
    }

    private static final long serialVersionUID = 1L;
}
