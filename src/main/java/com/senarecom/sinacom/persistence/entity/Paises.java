package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "pais_destino", schema = "parametrica")
public class Paises implements Serializable {


    private Long id;
    private String codigo;
    private String descripcion;
    @JsonGetter("code")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    @JsonGetter("description")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @JsonGetter("countryId")
    public Long getId() {
        return id;
    }
}
