package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="v_operadores_mineros_nim_vigente",schema = "transversal")
public class VOperadoresMineros implements Serializable {

    @Id
    @Column(name = "id_operador")
    private Long id_operador;
    private String numero_nim;
    private String razon_social;
    private String numero_nit;
    private String numero_ruex;
    private Date fecha_nim;
    private Date fecha_expiracion_nim;

//    @OneToOne(mappedBy = "vOperadoresMineros",fetch = FetchType.LAZY)
//    private VRepresentanteLegal vRepresentanteLegal;

    public Long getId_operador() {
        return id_operador;
    }

    public void setId_operador(Long id_operador) {
        this.id_operador = id_operador;
    }

    public String getNumero_nim() {
        return numero_nim;
    }

    public void setNumero_nim(String numero_nim) {
        this.numero_nim = numero_nim;
    }

    public String getRazon_social() {
        return razon_social;
    }

    public void setRazon_social(String razon_social) {
        this.razon_social = razon_social;
    }

    public String getNumero_nit() {
        return numero_nit;
    }

    public void setNumero_nit(String numero_nit) {
        this.numero_nit = numero_nit;
    }

    public String getNumero_ruex() {
        return numero_ruex;
    }

    public void setNumero_ruex(String numero_ruex) {
        this.numero_ruex = numero_ruex;
    }

    public Date getFecha_nim() {
        return fecha_nim;
    }

    public void setFecha_nim(Date fecha_nim) {
        this.fecha_nim = fecha_nim;
    }

    public Date getFecha_expiracion_nim() {
        return fecha_expiracion_nim;
    }

    public void setFecha_expiracion_nim(Date fecha_expiracion_nim) {
        this.fecha_expiracion_nim = fecha_expiracion_nim;
    }

//    public VRepresentanteLegal getvRepresentanteLegal() {
//        return vRepresentanteLegal;
//    }
//
//    public void setvRepresentanteLegal(VRepresentanteLegal vRepresentanteLegal) {
//        this.vRepresentanteLegal = vRepresentanteLegal;
//    }
}
