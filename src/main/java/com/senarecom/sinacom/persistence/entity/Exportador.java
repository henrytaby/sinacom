package com.senarecom.sinacom.persistence.entity;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "exportador",schema = "exportacion")
public class Exportador {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer idExportador;

    @Column(name="numero_nim")
    private String numeroNim;

    @Column(name="numero_nit")
    private String numeroNit;

    @Column(name="razon_social")
    private String razonSocial;

    @Column(name="numero_ruex")
    private String numeroRuex;






    @JsonGetter("exporterId")
    public Integer getIdExportador() {
        return idExportador;
    }

    public void setIdExportador(Integer idExportador) {
        this.idExportador = idExportador;
    }
    @JsonGetter("nimNumber")
    public String getNumeroNim() {
        return numeroNim;
    }

    public void setNumeroNim(String numeroNim) {
        this.numeroNim = numeroNim;
    }
    @JsonGetter("nitNumber")
    public String getNumeroNit() {
        return numeroNit;
    }

    public void setNumeroNit(String numeroNit) {
        this.numeroNit = numeroNit;
    }
    @JsonGetter("businessName")
    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }
    @JsonGetter("ruexNumber")
    public String getNumeroRuex() {
        return numeroRuex;
    }

    public void setNumeroRuex(String numeroRuex) {
        this.numeroRuex = numeroRuex;
    }
}
