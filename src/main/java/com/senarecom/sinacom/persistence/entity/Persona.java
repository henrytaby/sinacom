package com.senarecom.sinacom.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name="persona",schema = "seguridad")
public class Persona implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nombres;
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Column(name = "apellido_materno")
    private String apellidoMaterno;
    @Column(name = "apellido_casada")
    private String apellidoCasada;
    @Column(name = "fecha_nacimiento")
    private String fechaNacimiento;
    @Column(name = "id_tipo_documento")
    private Long idTipoDocumento;
    @Column(unique = true,name = "numero_documento")
    private String numeroDocumento;
    @Column(name = "id_lugar_emision_documento")
    private Long idLugarEmisionDocumento;
    private String telefono;
    private String celular;
    private Boolean activo;
    @Column(name = "usuario_registro")
    private String usuarioRegistro;
    @Column(name="fecha_registro")
    @Temporal(TemporalType.DATE)
    private Date fechaRegistro;
    @Column(name="fecha_ultima_modificacion")
    private Date fechaUltimaModificacion;
    @Column(name="correo_electronico")
    private String correoElectronico;

    @OneToOne(mappedBy = "persona")
    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    @OneToOne(mappedBy = "persona")
    private RepresentanteExportador representanteExportador;

    @OneToOne(mappedBy = "persona")
    private ServidorPublico servidorPublico;

    public ServidorPublico getServidorPublico() {
        return servidorPublico;
    }

    public void setServidorPublico(ServidorPublico servidorPublico) {
        this.servidorPublico = servidorPublico;
    }

    public RepresentanteExportador getRepresentanteExportador() {
        return representanteExportador;
    }

    public void setRepresentanteExportador(RepresentanteExportador representanteExportador) {
        this.representanteExportador = representanteExportador;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoCasada() {
        return apellidoCasada;
    }

    public void setApellidoCasada(String apellidoCasada) {
        this.apellidoCasada = apellidoCasada;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Long getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Long idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Long getIdLugarEmisionDocumento() {
        return idLugarEmisionDocumento;
    }

    public void setIdLugarEmisionDocumento(Long idLugarEmisionDocumento) {
        this.idLugarEmisionDocumento = idLugarEmisionDocumento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(String usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Date getFechaUltimaModificacion() {
        return fechaUltimaModificacion;
    }

    public void setFechaUltimaModificacion(Date fechaUltimaModificacion) {
        this.fechaUltimaModificacion = fechaUltimaModificacion;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }



    private static final long serialVersionUID = 1L;

}
